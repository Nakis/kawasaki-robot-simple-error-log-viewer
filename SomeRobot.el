.ERRLOG
   1 - [21/11/09 02:09:35 SIGNAL:00 MON.SPEED : 50 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/09 02:06:47] ( SIGNAL -12 )
       OPERATION2:[21/11/09 02:06:25] ( SIGNAL 12 )
       OPERATION3:[21/11/09 02:03:25] ( SIGNAL -12 )
       OPERATION4:[21/11/09 02:02:19] ( SIGNAL 12 )
       OPERATION5:[21/11/09 02:00:03] ( SIGNAL -12 )
       OPERATION6:[21/11/09 01:59:20] ( SIGNAL 12 )
       OPERATION7:[21/11/09 01:57:48] ( SIGNAL -12 )
       OPERATION8:[21/11/09 01:57:25] ( SIGNAL 12 )
       OPERATION9:[21/11/09 01:57:15] ( khidl1 disconnect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 64 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 108 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -44.437    25.069    -3.695   165.358     0.000     0.000 47432.426

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      39701.402

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -44.437    25.069    -3.695   165.358     0.000     0.000 47432.426

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      39701.402

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -44.437    25.069    -3.695   165.358     0.000     0.000 47432.426

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      39701.402

------------------------------------------------------------------------------
   2 - [21/11/09 00:55:09 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/09 00:51:10] ( ALLERESET )
       OPERATION2:[21/11/09 00:50:32] ( ALLERESET )
       OPERATION3:[21/11/09 00:47:22] ( Motor power ON )
       OPERATION4:[21/11/09 00:45:43] ( password = 1234 )
       OPERATION5:[21/11/09 00:44:42] ( REPEAT->TEACH )
       OPERATION6:[21/11/09 00:30:33] ( CYCLE START )
       OPERATION7:[21/11/09 00:30:33] ( PROGRAM RESET )
       OPERATION8:[21/11/09 00:30:33] ( Motor power ON )
       OPERATION9:[21/11/09 00:28:16] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:163 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 101 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 108 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -44.473     1.412     4.365    90.046     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      11697.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -44.488     1.411     4.366    90.025     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      11697.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
   3 - [21/11/09 00:51:07 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/11/09 00:50:32] ( ALLERESET )
       OPERATION2:[21/11/09 00:47:22] ( Motor power ON )
       OPERATION3:[21/11/09 00:45:43] ( password = 1234 )
       OPERATION4:[21/11/09 00:44:42] ( REPEAT->TEACH )
       OPERATION5:[21/11/09 00:30:33] ( CYCLE START )
       OPERATION6:[21/11/09 00:30:33] ( PROGRAM RESET )
       OPERATION7:[21/11/09 00:30:33] ( Motor power ON )
       OPERATION8:[21/11/09 00:28:16] ( EMERGENCY STOP )
       OPERATION9:[21/11/09 00:28:13] ( Control Power ON )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:163 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -22.710    10.629    50.239    90.025     0.000     0.000  1115.052

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      11697.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -22.706    10.632    50.238    90.025     0.000     0.000  1115.052

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      11697.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
   4 - [21/11/09 00:50:30 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/11/09 00:47:22] ( Motor power ON )
       OPERATION2:[21/11/09 00:45:43] ( password = 1234 )
       OPERATION3:[21/11/09 00:44:42] ( REPEAT->TEACH )
       OPERATION4:[21/11/09 00:30:33] ( CYCLE START )
       OPERATION5:[21/11/09 00:30:33] ( PROGRAM RESET )
       OPERATION6:[21/11/09 00:30:33] ( Motor power ON )
       OPERATION7:[21/11/09 00:28:16] ( EMERGENCY STOP )
       OPERATION8:[21/11/09 00:28:13] ( Control Power ON )
       OPERATION9:[21/11/09 00:28:12] ( webml1 connect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:163 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.568     5.329    47.105    90.025     0.000     0.000  1115.052

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      11697.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.654     6.509    47.951    90.025     0.000     0.000  1115.052

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      11697.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
   5 - [21/11/09 00:27:33 SIGNAL:00 MON.SPEED : 10 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/09 00:05:34] ( CYCLE START )
       OPERATION2:[21/11/09 00:05:34] ( PROGRAM RESET )
       OPERATION3:[21/11/09 00:05:34] ( Motor power ON )
       OPERATION4:[21/11/09 00:05:33] ( as )
       OPERATION5:[21/11/09 00:05:08] ( EMERGENCY STOP )
       OPERATION6:[21/11/09 00:05:05] ( Control Power ON )
       OPERATION7:[21/11/09 00:05:04] ( webml1 connect )
       OPERATION8:[21/11/08 23:46:11] ( SPEED 10 )
       OPERATION9:[21/11/08 17:29:35] ( CYCLE START )
       ROBOT1:
        PROGRAM:main Step:146 Cur_Step:146 STATUS:RUN
        Interpolation Type:LINEAR  S_parameter:0.000000
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.480    -9.890    89.998     0.000     0.000    74.285

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       2185.500

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000    74.285

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       2185.500

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
   6 - [21/11/09 00:04:31 SIGNAL:00 MON.SPEED : 10 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/08 23:46:11] ( SPEED 10 )
       OPERATION2:[21/11/08 17:29:35] ( CYCLE START )
       OPERATION3:[21/11/08 17:29:35] ( PROGRAM RESET )
       OPERATION4:[21/11/08 17:29:35] ( Motor power ON )
       OPERATION5:[21/11/08 17:28:58] ( EMERGENCY STOP )
       OPERATION6:[21/11/08 17:28:55] ( Control Power ON )
       OPERATION7:[21/11/08 17:28:54] ( webml1 connect )
       OPERATION8:[21/11/08 17:22:37] ( CYCLE START )
       OPERATION9:[21/11/08 17:22:36] ( PROGRAM RESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:147 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 78 STATUS: RUN
       PC2 PROGRAM: vision_recv.pc Step No: 13 STATUS: RUN
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.474    -9.892    90.184     0.000     0.000 31193.359

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       2185.500

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000 31193.359

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       2185.500

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
   7 - [21/11/08 17:28:22 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/08 17:22:37] ( CYCLE START )
       OPERATION2:[21/11/08 17:22:36] ( PROGRAM RESET )
       OPERATION3:[21/11/08 17:22:36] ( Motor power ON )
       OPERATION4:[21/11/08 16:06:55] ( EMERGENCY STOP )
       OPERATION5:[21/11/08 16:06:52] ( Control Power ON )
       OPERATION6:[21/11/08 16:06:51] ( webml1 connect )
       OPERATION7:[21/11/06 22:50:32] ( AUX1 disconnect )
       OPERATION8:[21/11/06 22:47:55] ( save/full Hologic_ )
       OPERATION9:[21/11/06 22:47:35] ( AUX1 connect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 221 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.860    33.474    -9.892    90.002     0.000     0.000302956.937

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      15976.501

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.860    33.474    -9.892    90.002     0.000     0.000302956.937

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      15976.501

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.860    33.474    -9.892    90.002     0.000     0.000302956.937

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      15976.501

------------------------------------------------------------------------------
   8 - [21/11/07 01:28:27 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/06 22:50:32] ( AUX1 disconnect )
       OPERATION2:[21/11/06 22:47:55] ( save/full Hologic_ )
       OPERATION3:[21/11/06 22:47:35] ( AUX1 connect )
       OPERATION4:[21/11/06 22:41:14] ( khidl1 disconnect )
       OPERATION5:[21/11/06 21:20:17] ( LOAD using.rcc )
       OPERATION6:[21/11/06 21:18:51] ( SAVE using.rcc )
       OPERATION7:[21/11/06 21:18:48] ( ID )
       OPERATION8:[21/11/06 21:18:48] ( TYPE DEXT(HERE,9) )
       OPERATION9:[21/11/06 21:18:47] ( TYPE DEXT(HERE,8) )
       ROBOT1:
        PROGRAM:main Step:123 Cur_Step:124 STATUS:RUN
        Interpolation Type:LINEAR  S_parameter:0.000000
       PC1 PROGRAM: autostart.pc Step No: 64 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.480    -9.890    89.998     0.000     0.000  1121.553

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      15976.501

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000  1121.553

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      15976.501

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
   9 - [21/11/06 19:46:36 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/06 17:22:53] ( EMERGENCY STOP )
       OPERATION2:[21/11/06 16:56:06] ( CYCLE START )
       OPERATION3:[21/11/06 16:56:05] ( PROGRAM RESET )
       OPERATION4:[21/11/06 16:56:05] ( Motor power ON )
       OPERATION5:[21/11/06 16:56:03] ( EMERGENCY STOP )
       OPERATION6:[21/11/06 16:49:40] ( CYCLE START )
       OPERATION7:[21/11/06 16:49:40] ( PROGRAM RESET )
       OPERATION8:[21/11/06 16:49:40] ( Motor power ON )
       OPERATION9:[21/11/06 16:49:37] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:147 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 221 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.474    -9.892    90.029     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       9914.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.474    -9.892    90.029     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       9914.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  10 - [21/11/06 01:07:58 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/05 23:41:23] ( CYCLE START )
       OPERATION2:[21/11/05 23:41:23] ( PROGRAM RESET )
       OPERATION3:[21/11/05 23:41:23] ( Motor power ON )
       OPERATION4:[21/11/05 23:39:44] ( password = 1234 )
       OPERATION5:[21/11/05 23:38:34] ( EMERGENCY STOP )
       OPERATION6:[21/11/05 23:38:31] ( Control Power ON )
       OPERATION7:[21/11/05 23:38:30] ( webml1 connect )
       OPERATION8:[21/11/05 23:37:39] ( EMERGENCY STOP )
       OPERATION9:[21/11/05 23:37:37] ( Control Power ON )
       ROBOT1:
        PROGRAM:main Step:335 Cur_Step:335 STATUS:RUN
        Interpolation Type:LINEAR  S_parameter:1.000000
       PC1 PROGRAM: string_window Step No: 101 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         12.606   -32.452   -12.181   179.939     0.000     0.000  5366.272

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       7169.100

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         12.606   -32.454   -12.183   179.939     0.000     0.000  5366.272

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       7169.100

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         12.606   -32.454   -12.183   179.939     0.000     0.000  4284.679

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       9120.601

------------------------------------------------------------------------------
  11 - [21/11/05 23:37:55 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/05 23:37:39] ( EMERGENCY STOP )
       OPERATION2:[21/11/05 23:37:37] ( Control Power ON )
       OPERATION3:[21/11/05 23:37:35] ( webml1 connect )
       OPERATION4:[21/11/05 23:36:57] ( ALLERESET )
       OPERATION5:[21/11/05 23:36:31] ( EMERGENCY STOP )
       OPERATION6:[21/11/05 23:36:29] ( Control Power ON )
       OPERATION7:[21/11/05 23:36:27] ( webml1 connect )
       OPERATION8:[21/11/05 23:35:03] ( password = 1234 )
       OPERATION9:[21/11/05 19:07:44] ( CYCLE START )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 86 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

------------------------------------------------------------------------------
  12 - [21/11/05 23:37:39 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (E1432)[MC1]Communication error for PN voltage sensor(Servo board 1)
       OPERATION1:[21/11/05 23:37:37] ( Control Power ON )
       OPERATION2:[21/11/05 23:37:35] ( webml1 connect )
       OPERATION3:[21/11/05 23:36:57] ( ALLERESET )
       OPERATION4:[21/11/05 23:36:31] ( EMERGENCY STOP )
       OPERATION5:[21/11/05 23:36:29] ( Control Power ON )
       OPERATION6:[21/11/05 23:36:27] ( webml1 connect )
       OPERATION7:[21/11/05 23:35:03] ( password = 1234 )
       OPERATION8:[21/11/05 19:07:44] ( CYCLE START )
       OPERATION9:[21/11/05 19:07:44] ( PROGRAM RESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

------------------------------------------------------------------------------
  13 - [21/11/05 23:37:02 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/05 23:36:57] ( ALLERESET )
       OPERATION2:[21/11/05 23:36:31] ( EMERGENCY STOP )
       OPERATION3:[21/11/05 23:36:29] ( Control Power ON )
       OPERATION4:[21/11/05 23:36:27] ( webml1 connect )
       OPERATION5:[21/11/05 23:35:03] ( password = 1234 )
       OPERATION6:[21/11/05 19:07:44] ( CYCLE START )
       OPERATION7:[21/11/05 19:07:44] ( PROGRAM RESET )
       OPERATION8:[21/11/05 19:07:44] ( Motor power ON )
       OPERATION9:[21/11/05 19:07:40] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 134 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

------------------------------------------------------------------------------
  14 - [21/11/05 23:36:57 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (E1432)[MC1]Communication error for PN voltage sensor(Servo board 1)
       OPERATION1:[21/11/05 23:36:57] ( ALLERESET )
       OPERATION2:[21/11/05 23:36:31] ( EMERGENCY STOP )
       OPERATION3:[21/11/05 23:36:29] ( Control Power ON )
       OPERATION4:[21/11/05 23:36:27] ( webml1 connect )
       OPERATION5:[21/11/05 23:35:03] ( password = 1234 )
       OPERATION6:[21/11/05 19:07:44] ( CYCLE START )
       OPERATION7:[21/11/05 19:07:44] ( PROGRAM RESET )
       OPERATION8:[21/11/05 19:07:44] ( Motor power ON )
       OPERATION9:[21/11/05 19:07:40] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

------------------------------------------------------------------------------
  15 - [21/11/05 23:36:31 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (E1432)[MC1]Communication error for PN voltage sensor(Servo board 1)
       OPERATION1:[21/11/05 23:36:29] ( Control Power ON )
       OPERATION2:[21/11/05 23:36:27] ( webml1 connect )
       OPERATION3:[21/11/05 23:35:03] ( password = 1234 )
       OPERATION4:[21/11/05 19:07:44] ( CYCLE START )
       OPERATION5:[21/11/05 19:07:44] ( PROGRAM RESET )
       OPERATION6:[21/11/05 19:07:44] ( Motor power ON )
       OPERATION7:[21/11/05 19:07:40] ( EMERGENCY STOP )
       OPERATION8:[21/11/05 17:39:58] ( Motor power ON )
       OPERATION9:[21/11/05 16:55:37] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

------------------------------------------------------------------------------
  16 - [21/11/05 23:35:52 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/05 23:35:03] ( password = 1234 )
       OPERATION2:[21/11/05 19:07:44] ( CYCLE START )
       OPERATION3:[21/11/05 19:07:44] ( PROGRAM RESET )
       OPERATION4:[21/11/05 19:07:44] ( Motor power ON )
       OPERATION5:[21/11/05 19:07:40] ( EMERGENCY STOP )
       OPERATION6:[21/11/05 17:39:58] ( Motor power ON )
       OPERATION7:[21/11/05 16:55:37] ( EMERGENCY STOP )
       OPERATION8:[21/11/05 16:55:34] ( Control Power ON )
       OPERATION9:[21/11/05 16:55:33] ( webml1 connect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:146 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 64 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.023     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000 45314.570

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1231.800

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  17 - [21/11/05 16:54:59 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/05 16:25:05] ( EMERGENCY STOP )
       OPERATION2:[21/11/05 15:57:42] ( EMERGENCY STOP )
       OPERATION3:[21/11/05 15:57:39] ( Control Power ON )
       OPERATION4:[21/11/05 15:57:38] ( webml1 connect )
       OPERATION5:[21/11/04 19:41:52] ( CYCLE START )
       OPERATION6:[21/11/04 19:41:52] ( PROGRAM RESET )
       OPERATION7:[21/11/04 19:41:52] ( Motor power ON )
       OPERATION8:[21/11/04 18:46:11] ( as )
       OPERATION9:[21/11/04 18:45:46] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:146 Cur_Step:147 STATUS:RUN
        Interpolation Type:LINEAR  S_parameter:0.000000
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.480    -9.890    89.998     0.000     0.0001534152.37

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       9832.200

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.0001534152.37

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       9832.200

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  18 - [21/11/05 01:52:58 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/04 19:41:52] ( CYCLE START )
       OPERATION2:[21/11/04 19:41:52] ( PROGRAM RESET )
       OPERATION3:[21/11/04 19:41:52] ( Motor power ON )
       OPERATION4:[21/11/04 18:46:11] ( as )
       OPERATION5:[21/11/04 18:45:46] ( EMERGENCY STOP )
       OPERATION6:[21/11/04 18:45:43] ( Control Power ON )
       OPERATION7:[21/11/04 18:45:42] ( webml1 connect )
       OPERATION8:[21/11/04 16:52:10] ( CYCLE START )
       OPERATION9:[21/11/04 16:52:10] ( PROGRAM RESET )
       ROBOT1:
        PROGRAM:main Step:146 Cur_Step:147 STATUS:RUN
        Interpolation Type:LINEAR  S_parameter:0.000000
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.480    -9.890    89.998     0.000     0.0001465406.00

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       9832.200

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.0001465406.00

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       9832.200

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  19 - [21/11/04 18:45:09 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/04 16:52:10] ( CYCLE START )
       OPERATION2:[21/11/04 16:52:10] ( PROGRAM RESET )
       OPERATION3:[21/11/04 16:52:10] ( Motor power ON )
       OPERATION4:[21/11/04 16:10:27] ( EMERGENCY STOP )
       OPERATION5:[21/11/04 16:10:25] ( Control Power ON )
       OPERATION6:[21/11/04 16:10:23] ( webml1 connect )
       OPERATION7:[21/11/03 20:10:52] ( Control Power ON )
       OPERATION8:[21/11/03 20:10:50] ( webml1 connect )
       OPERATION9:[21/11/03 16:09:08] ( CYCLE START )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 221 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.860    33.474    -9.892    90.021     0.000     0.000160976.719

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        935.100

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.021     0.000     0.000160976.719

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        935.100

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.860    33.474    -9.892    90.021     0.000     0.000160976.719

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        935.100

------------------------------------------------------------------------------
  20 - [21/11/04 00:50:29 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/03 20:10:52] ( Control Power ON )
       OPERATION2:[21/11/03 20:10:50] ( webml1 connect )
       OPERATION3:[21/11/03 16:09:08] ( CYCLE START )
       OPERATION4:[21/11/03 16:09:07] ( PROGRAM RESET )
       OPERATION5:[21/11/03 16:09:07] ( Motor power ON )
       OPERATION6:[21/11/03 16:06:17] ( as )
       OPERATION7:[21/11/03 16:05:52] ( EMERGENCY STOP )
       OPERATION8:[21/11/03 16:05:50] ( Control Power ON )
       OPERATION9:[21/11/03 16:05:48] ( webml1 connect )
       ROBOT1:
        PROGRAM:main Step:146 Cur_Step:147 STATUS:RUN
        Interpolation Type:LINEAR  S_parameter:0.000000
       PC1 PROGRAM: conv_control.pc Step No: 151 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.480    -9.890    89.998     0.000     0.000132410.813

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      21267.602

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000132410.813

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      21267.602

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  21 - [21/11/03 20:10:19 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/03 16:09:08] ( CYCLE START )
       OPERATION2:[21/11/03 16:09:07] ( PROGRAM RESET )
       OPERATION3:[21/11/03 16:09:07] ( Motor power ON )
       OPERATION4:[21/11/03 16:06:17] ( as )
       OPERATION5:[21/11/03 16:05:52] ( EMERGENCY STOP )
       OPERATION6:[21/11/03 16:05:50] ( Control Power ON )
       OPERATION7:[21/11/03 16:05:48] ( webml1 connect )
       OPERATION8:[21/11/02 22:18:42] ( CYCLE START )
       OPERATION9:[21/11/02 22:18:42] ( PROGRAM RESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 226 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.860    33.474    -9.892    90.019     0.000     0.000132410.891

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      16959.301

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.013     0.000     0.000132410.891

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      16959.301

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.860    33.474    -9.892    90.019     0.000     0.000132410.891

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      16959.301

------------------------------------------------------------------------------
  22 - [21/11/03 17:51:38 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (E0102)Variable is not defined.
       OPERATION1:[21/11/03 16:09:08] ( CYCLE START )
       OPERATION2:[21/11/03 16:09:07] ( PROGRAM RESET )
       OPERATION3:[21/11/03 16:09:07] ( Motor power ON )
       OPERATION4:[21/11/03 16:06:17] ( as )
       OPERATION5:[21/11/03 16:05:52] ( EMERGENCY STOP )
       OPERATION6:[21/11/03 16:05:50] ( Control Power ON )
       OPERATION7:[21/11/03 16:05:48] ( webml1 connect )
       OPERATION8:[21/11/02 22:18:42] ( CYCLE START )
       OPERATION9:[21/11/02 22:18:42] ( PROGRAM RESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:53 STATUS:STOP
       PC1 PROGRAM: conv_control.pc Step No: 26 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.860    33.476    -9.894    90.010     0.000     0.000132410.891

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      14251.801

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.013     0.000     0.000132410.891

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      14251.801

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.860    33.476    -9.894    90.010     0.000     0.000132410.891

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      14251.801

------------------------------------------------------------------------------
  23 - [21/11/02 23:43:30 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/02 22:18:42] ( CYCLE START )
       OPERATION2:[21/11/02 22:18:42] ( PROGRAM RESET )
       OPERATION3:[21/11/02 22:18:42] ( Motor power ON )
       OPERATION4:[21/11/02 22:11:30] ( EMERGENCY STOP )
       OPERATION5:[21/11/02 22:11:16] ( CYCLE START )
       OPERATION6:[21/11/02 22:11:16] ( PROGRAM RESET )
       OPERATION7:[21/11/02 22:11:16] ( Motor power ON )
       OPERATION8:[21/11/02 22:11:11] ( EMERGENCY STOP )
       OPERATION9:[21/11/02 21:30:12] ( CYCLE START )
       ROBOT1:
        PROGRAM:main Step:123 Cur_Step:124 STATUS:RUN
        Interpolation Type:LINEAR  S_parameter:0.000000
       PC1 PROGRAM: conv_control.pc Step No: 151 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.480    -9.890    89.998     0.000     0.000   851.804

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      14251.801

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000   851.804

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      14251.801

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  24 - [21/11/02 21:06:35 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/02 20:19:55] ( EMERGENCY STOP )
       OPERATION2:[21/11/02 19:27:18] ( CYCLE START )
       OPERATION3:[21/11/02 19:27:18] ( PROGRAM RESET )
       OPERATION4:[21/11/02 19:27:18] ( Motor power ON )
       OPERATION5:[21/11/02 19:27:16] ( EMERGENCY STOP )
       OPERATION6:[21/11/02 19:11:11] ( CYCLE START )
       OPERATION7:[21/11/02 19:11:11] ( PROGRAM RESET )
       OPERATION8:[21/11/02 19:11:11] ( Motor power ON )
       OPERATION9:[21/11/02 19:08:03] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:163 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.021     0.000     0.000  7492.272

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        261.600

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.021     0.000     0.000  7492.272

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        261.600

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  25 - [21/11/02 19:00:05 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/02 19:00:01] ( EMERGENCY STOP )
       OPERATION2:[21/11/02 18:59:28] ( CYCLE START )
       OPERATION3:[21/11/02 18:59:27] ( PROGRAM RESET )
       OPERATION4:[21/11/02 18:59:27] ( Motor power ON )
       OPERATION5:[21/11/02 18:59:25] ( EMERGENCY STOP )
       OPERATION6:[21/11/02 18:59:24] ( CYCLE START )
       OPERATION7:[21/11/02 18:59:24] ( PROGRAM RESET )
       OPERATION8:[21/11/02 18:59:24] ( Motor power ON )
       OPERATION9:[21/11/02 18:58:52] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:147 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 226 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.038     0.000     0.000108179.680

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      22566.301

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.038     0.000     0.000108179.680

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      22566.301

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  26 - [21/11/02 01:48:43 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/02 00:27:14] ( CYCLE START )
       OPERATION2:[21/11/02 00:27:14] ( PROGRAM RESET )
       OPERATION3:[21/11/02 00:27:14] ( Motor power ON )
       OPERATION4:[21/11/02 00:27:11] ( Control Power ON )
       OPERATION5:[21/11/02 00:27:09] ( webml1 connect )
       OPERATION6:[21/11/02 00:25:08] ( ALLERESET )
       OPERATION7:[21/11/01 20:22:59] ( khidl1 disconnect )
       OPERATION8:[21/11/01 19:42:49] ( password = 1234 )
       OPERATION9:[21/11/01 19:42:36] ( prime main )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:146 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.474    -9.892    90.090     0.000     0.000 11622.314

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      22566.301

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000 11622.314

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      22566.301

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  27 - [21/11/02 00:26:29 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/11/02 00:25:08] ( ALLERESET )
       OPERATION2:[21/11/01 20:22:59] ( khidl1 disconnect )
       OPERATION3:[21/11/01 19:42:49] ( password = 1234 )
       OPERATION4:[21/11/01 19:42:36] ( prime main )
       OPERATION5:[21/11/01 19:42:25] ( LOAD using.rcc )
       OPERATION6:[21/11/01 19:42:24] ( TYPE TASK (1) )
       OPERATION7:[21/11/01 19:42:24] ( KILL )
       OPERATION8:[21/11/01 19:42:24] ( ABORT )
       OPERATION9:[21/11/01 19:41:50] ( SAVE using.rcc )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 226 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.474    -9.892    90.107     0.000     0.000 26449.623

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4188.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000 26449.623

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4188.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.474    -9.892    90.107     0.000     0.000 26449.623

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4188.900

------------------------------------------------------------------------------
  28 - [21/10/29 22:16:30 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/29 21:59:19] ( AUX1 disconnect )
       OPERATION2:[21/10/29 21:58:58] ( save/full Hologic_ )
       OPERATION3:[21/10/29 21:58:41] ( AUX1 connect )
       OPERATION4:[21/10/29 20:08:34] ( CYCLE START )
       OPERATION5:[21/10/29 20:08:34] ( PROGRAM RESET )
       OPERATION6:[21/10/29 20:08:33] ( TEACH->REPEAT )
       OPERATION7:[21/10/29 20:08:33] ( Motor power ON )
       OPERATION8:[21/10/29 20:07:45] ( Motor power ON )
       OPERATION9:[21/10/29 20:07:19] ( REPEAT->TEACH )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:147 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.860    33.476    -9.892    90.004     0.000     0.000 23120.504

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25692.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000 23120.504

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25692.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  29 - [21/10/29 00:20:53 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/28 22:47:08] ( CYCLE START )
       OPERATION2:[21/10/28 22:47:08] ( PROGRAM RESET )
       OPERATION3:[21/10/28 22:47:08] ( Motor power ON )
       OPERATION4:[21/10/28 22:46:43] ( as )
       OPERATION5:[21/10/28 22:46:18] ( EMERGENCY STOP )
       OPERATION6:[21/10/28 22:46:15] ( Control Power ON )
       OPERATION7:[21/10/28 22:46:14] ( webml1 connect )
       OPERATION8:[21/10/28 21:17:23] ( AUX1 disconnect )
       OPERATION9:[21/10/28 21:16:49] ( save/full Hologic_ )
       ROBOT1:
        PROGRAM:main Step:146 Cur_Step:148 STATUS:WAIT
        Interpolation Type:LINEAR  S_parameter:0.000000
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.480    -9.890    89.998     0.000     0.000  2984.133

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       5223.600

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000  2984.133

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       5223.600

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  30 - [21/10/28 22:37:42 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/28 21:17:23] ( AUX1 disconnect )
       OPERATION2:[21/10/28 21:16:49] ( save/full Hologic_ )
       OPERATION3:[21/10/28 21:16:30] ( AUX1 connect )
       OPERATION4:[21/10/28 20:45:51] ( CYCLE START )
       OPERATION5:[21/10/28 20:45:51] ( PROGRAM RESET )
       OPERATION6:[21/10/28 20:45:51] ( Motor power ON )
       OPERATION7:[21/10/28 20:45:46] ( EMERGENCY STOP )
       OPERATION8:[21/10/28 20:44:21] ( CYCLE START )
       OPERATION9:[21/10/28 20:44:21] ( PROGRAM RESET )
       ROBOT1:
        PROGRAM:check_area Step:146 Cur_Step:154 STATUS:STOP
        Interpolation Type:LINEAR  S_parameter:0.000000
       PC1 PROGRAM: string_window Step No: 101 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.480    -9.890    89.998     0.000     0.000  2984.133

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       5223.600

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000  2984.133

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       5223.600

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  31 - [21/10/28 19:15:44 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/28 19:15:32] ( ALLERESET )
       OPERATION2:[21/10/28 19:15:07] ( Motor power ON )
       OPERATION3:[21/10/28 19:14:25] ( REPEAT->TEACH )
       OPERATION4:[21/10/28 19:14:00] ( ALLERESET )
       OPERATION5:[21/10/28 19:10:43] ( TEACH->REPEAT )
       OPERATION6:[21/10/28 19:08:24] ( ALLERESET )
       OPERATION7:[21/10/28 19:08:16] ( ALLERESET )
       OPERATION8:[21/10/28 19:07:32] ( Motor power ON )
       OPERATION9:[21/10/28 19:07:25] ( password = 1234 )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:148 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 90 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         39.037   -36.161    55.383    90.008     0.000     0.000  4182.397

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      14881.200

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         39.395   -36.054    55.862    90.008     0.000     0.000  4182.397

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      14881.200

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  32 - [21/10/28 19:15:30 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/28 19:15:07] ( Motor power ON )
       OPERATION2:[21/10/28 19:14:25] ( REPEAT->TEACH )
       OPERATION3:[21/10/28 19:14:00] ( ALLERESET )
       OPERATION4:[21/10/28 19:10:43] ( TEACH->REPEAT )
       OPERATION5:[21/10/28 19:08:24] ( ALLERESET )
       OPERATION6:[21/10/28 19:08:16] ( ALLERESET )
       OPERATION7:[21/10/28 19:07:32] ( Motor power ON )
       OPERATION8:[21/10/28 19:07:25] ( password = 1234 )
       OPERATION9:[21/10/28 19:07:03] ( REPEAT->TEACH )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:148 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -4.528   -11.404    69.314    90.008     0.000     0.000  4182.397

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      14881.200

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -4.086   -11.315    69.779    90.008     0.000     0.000  4182.397

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      14881.200

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  33 - [21/10/28 19:13:37 SIGNAL:00 MON.SPEED : 10 REPEAT mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/28 19:10:43] ( TEACH->REPEAT )
       OPERATION2:[21/10/28 19:08:24] ( ALLERESET )
       OPERATION3:[21/10/28 19:08:16] ( ALLERESET )
       OPERATION4:[21/10/28 19:07:32] ( Motor power ON )
       OPERATION5:[21/10/28 19:07:25] ( password = 1234 )
       OPERATION6:[21/10/28 19:07:03] ( REPEAT->TEACH )
       OPERATION7:[21/10/28 19:06:47] ( SPEED 10 )
       OPERATION8:[21/10/28 19:05:18] ( ALLERESET )
       OPERATION9:[21/10/28 19:05:00] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:391 STATUS:STOP
       PC1 PROGRAM: conv_control.pc Step No: 88 STATUS: RUN
       PC2 PROGRAM: vision_recv.pc Step No: 13 STATUS: RUN
       PC3 PROGRAM: feed_out_con.pc Step No: 21 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         47.739   -29.667    59.624   180.002     0.000     0.000  1003.829

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1204.843

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         47.769   -29.658    59.678   180.003     0.000     0.000  1003.829

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1204.843

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         16.607   -21.299   -10.084   180.003     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  34 - [21/10/28 19:08:22 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/28 19:08:16] ( ALLERESET )
       OPERATION2:[21/10/28 19:07:32] ( Motor power ON )
       OPERATION3:[21/10/28 19:07:25] ( password = 1234 )
       OPERATION4:[21/10/28 19:07:03] ( REPEAT->TEACH )
       OPERATION5:[21/10/28 19:06:47] ( SPEED 10 )
       OPERATION6:[21/10/28 19:05:18] ( ALLERESET )
       OPERATION7:[21/10/28 19:05:00] ( ALLERESET )
       OPERATION8:[21/10/28 19:04:00] ( ALLERESET )
       OPERATION9:[21/10/28 19:03:01] ( SPEED 100 )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:147 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 128 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -5.108   -11.530    68.712    90.002     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25484.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -4.380   -11.382    69.475    90.002     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25484.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  35 - [21/10/28 19:07:46 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/28 19:07:32] ( Motor power ON )
       OPERATION2:[21/10/28 19:07:25] ( password = 1234 )
       OPERATION3:[21/10/28 19:07:03] ( REPEAT->TEACH )
       OPERATION4:[21/10/28 19:06:47] ( SPEED 10 )
       OPERATION5:[21/10/28 19:05:18] ( ALLERESET )
       OPERATION6:[21/10/28 19:05:00] ( ALLERESET )
       OPERATION7:[21/10/28 19:04:00] ( ALLERESET )
       OPERATION8:[21/10/28 19:03:01] ( SPEED 100 )
       OPERATION9:[21/10/28 18:58:36] ( SPEED 50 )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:147 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 100 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -4.624   -11.426    69.214    90.002     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25484.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -4.127   -11.325    69.737    90.002     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25484.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  36 - [21/10/28 19:05:03 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (E1088)Destination is out of motion range.
       OPERATION1:[21/10/28 19:05:00] ( ALLERESET )
       OPERATION2:[21/10/28 19:04:00] ( ALLERESET )
       OPERATION3:[21/10/28 19:03:01] ( SPEED 100 )
       OPERATION4:[21/10/28 18:58:36] ( SPEED 50 )
       OPERATION5:[21/10/28 18:55:54] ( SPEED 10 )
       OPERATION6:[21/10/28 18:48:17] ( prime main )
       OPERATION7:[21/10/28 18:48:11] ( LOAD using.rcc )
       OPERATION8:[21/10/28 18:48:11] ( TYPE TASK (1) )
       OPERATION9:[21/10/28 18:48:11] ( KILL )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:389 STATUS:STOP
       PC1 PROGRAM: conv_control.pc Step No: 15 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         47.946   -29.609    59.911   179.931     0.000     0.000  4829.030

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      18654.943

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         47.946   -29.609    59.911   179.935     0.000     0.000  4829.030

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      18654.943

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         32.470   -32.712    35.805   180.003     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  37 - [21/10/28 19:04:37 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/28 19:04:00] ( ALLERESET )
       OPERATION2:[21/10/28 19:03:01] ( SPEED 100 )
       OPERATION3:[21/10/28 18:58:36] ( SPEED 50 )
       OPERATION4:[21/10/28 18:55:54] ( SPEED 10 )
       OPERATION5:[21/10/28 18:48:17] ( prime main )
       OPERATION6:[21/10/28 18:48:11] ( LOAD using.rcc )
       OPERATION7:[21/10/28 18:48:11] ( TYPE TASK (1) )
       OPERATION8:[21/10/28 18:48:11] ( KILL )
       OPERATION9:[21/10/28 18:48:11] ( ABORT )
       ROBOT1:
        PROGRAM:main Step:382 Cur_Step:389 STATUS:WAIT
        Interpolation Type:LINEAR  S_parameter:0.000000
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 21 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         47.779   -29.665    59.708   180.010     0.000     0.000   972.673

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1194.043

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         47.950   -29.610    59.921   180.003     0.000     0.000   972.673

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1194.043

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         32.470   -32.712    35.805   180.003     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  38 - [21/10/28 19:03:56 SIGNAL:00 MON.SPEED : 100 REPEAT mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/28 19:03:01] ( SPEED 100 )
       OPERATION2:[21/10/28 18:58:36] ( SPEED 50 )
       OPERATION3:[21/10/28 18:55:54] ( SPEED 10 )
       OPERATION4:[21/10/28 18:48:17] ( prime main )
       OPERATION5:[21/10/28 18:48:11] ( LOAD using.rcc )
       OPERATION6:[21/10/28 18:48:11] ( TYPE TASK (1) )
       OPERATION7:[21/10/28 18:48:11] ( KILL )
       OPERATION8:[21/10/28 18:48:11] ( ABORT )
       OPERATION9:[21/10/28 18:41:47] ( prime main )
       ROBOT1:
        PROGRAM:main Step:391 Cur_Step:393 STATUS:RUN
        Interpolation Type:LINEAR  S_parameter:0.913194
       PC1 PROGRAM: conv_control.pc Step No: 72 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 21 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         48.793   -29.815    61.184   180.002     0.000     0.000   887.900

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1222.543

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         48.823   -29.835    61.231   180.003     0.000     0.000   887.900

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1222.543

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         44.204   -36.096    56.824   180.003     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1161.643

------------------------------------------------------------------------------
  39 - [21/10/28 18:06:30 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/28 16:10:55] ( khidl1 disconnect )
       OPERATION2:[21/10/28 16:10:47] ( LOAD using.rcc )
       OPERATION3:[21/10/28 16:10:18] ( SAVE using.rcc )
       OPERATION4:[21/10/28 16:10:14] ( ID )
       OPERATION5:[21/10/28 16:10:14] ( TYPE DEXT(HERE,9) )
       OPERATION6:[21/10/28 16:10:14] ( TYPE DEXT(HERE,8) )
       OPERATION7:[21/10/28 16:10:14] ( TYPE DEXT(HERE,7) )
       OPERATION8:[21/10/28 16:10:14] ( TYPE SYSDATA (Lang )
       OPERATION9:[21/10/28 16:10:14] ( MESSAGES ON )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 224 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.464    -9.890    88.818     0.000     0.000   110.529

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25096.287

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.464    -9.890    88.818     0.000     0.000   110.529

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25096.287

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.464    -9.890    88.818     0.000     0.000   110.529

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25096.287

------------------------------------------------------------------------------
  40 - [21/10/27 23:32:07 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/27 21:07:20] ( EMERGENCY STOP )
       OPERATION2:[21/10/27 20:46:36] ( EMERGENCY STOP )
       OPERATION3:[21/10/27 20:43:53] ( EMERGENCY STOP )
       OPERATION4:[21/10/27 20:43:51] ( Control Power ON )
       OPERATION5:[21/10/27 20:43:49] ( webml1 connect )
       OPERATION6:[21/10/27 20:42:52] ( ALLERESET )
       OPERATION7:[21/10/27 20:42:51] ( ALLERESET )
       OPERATION8:[21/10/27 20:42:43] ( Control Power ON )
       OPERATION9:[21/10/27 20:42:41] ( webml1 connect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:147 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 53 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.464    -9.890    88.816     0.000     0.000   288.904

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       3885.300

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.464    -9.890    88.816     0.000     0.000   288.904

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       3885.300

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  41 - [21/10/27 20:43:01 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/27 20:42:52] ( ALLERESET )
       OPERATION2:[21/10/27 20:42:51] ( ALLERESET )
       OPERATION3:[21/10/27 20:42:43] ( Control Power ON )
       OPERATION4:[21/10/27 20:42:41] ( webml1 connect )
       OPERATION5:[21/10/27 20:41:12] ( ALLERESET )
       OPERATION6:[21/10/27 20:41:11] ( ALLERESET )
       OPERATION7:[21/10/27 20:40:28] ( EMERGENCY STOP )
       OPERATION8:[21/10/27 20:40:26] ( Control Power ON )
       OPERATION9:[21/10/27 20:40:24] ( webml1 connect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 86 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

------------------------------------------------------------------------------
  42 - [21/10/27 20:42:52 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (E1432)[MC1]Communication error for PN voltage sensor(Servo board 1)
       OPERATION1:[21/10/27 20:42:52] ( ALLERESET )
       OPERATION2:[21/10/27 20:42:51] ( ALLERESET )
       OPERATION3:[21/10/27 20:42:43] ( Control Power ON )
       OPERATION4:[21/10/27 20:42:41] ( webml1 connect )
       OPERATION5:[21/10/27 20:41:12] ( ALLERESET )
       OPERATION6:[21/10/27 20:41:11] ( ALLERESET )
       OPERATION7:[21/10/27 20:40:28] ( EMERGENCY STOP )
       OPERATION8:[21/10/27 20:40:26] ( Control Power ON )
       OPERATION9:[21/10/27 20:40:24] ( webml1 connect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

------------------------------------------------------------------------------
  43 - [21/10/27 20:42:51 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (E1432)[MC1]Communication error for PN voltage sensor(Servo board 1)
       OPERATION1:[21/10/27 20:42:51] ( ALLERESET )
       OPERATION2:[21/10/27 20:42:43] ( Control Power ON )
       OPERATION3:[21/10/27 20:42:41] ( webml1 connect )
       OPERATION4:[21/10/27 20:41:12] ( ALLERESET )
       OPERATION5:[21/10/27 20:41:11] ( ALLERESET )
       OPERATION6:[21/10/27 20:40:28] ( EMERGENCY STOP )
       OPERATION7:[21/10/27 20:40:26] ( Control Power ON )
       OPERATION8:[21/10/27 20:40:24] ( webml1 connect )
       OPERATION9:[21/10/27 20:39:07] ( TEACH->REPEAT )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 72 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

------------------------------------------------------------------------------
  44 - [21/10/27 20:42:45 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (E1432)[MC1]Communication error for PN voltage sensor(Servo board 1)
       OPERATION1:[21/10/27 20:42:43] ( Control Power ON )
       OPERATION2:[21/10/27 20:42:41] ( webml1 connect )
       OPERATION3:[21/10/27 20:41:12] ( ALLERESET )
       OPERATION4:[21/10/27 20:41:11] ( ALLERESET )
       OPERATION5:[21/10/27 20:40:28] ( EMERGENCY STOP )
       OPERATION6:[21/10/27 20:40:26] ( Control Power ON )
       OPERATION7:[21/10/27 20:40:24] ( webml1 connect )
       OPERATION8:[21/10/27 20:39:07] ( TEACH->REPEAT )
       OPERATION9:[21/10/27 20:38:26] ( REPEAT->TEACH )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

------------------------------------------------------------------------------
  45 - [21/10/27 20:42:08 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/27 20:41:12] ( ALLERESET )
       OPERATION2:[21/10/27 20:41:11] ( ALLERESET )
       OPERATION3:[21/10/27 20:40:28] ( EMERGENCY STOP )
       OPERATION4:[21/10/27 20:40:26] ( Control Power ON )
       OPERATION5:[21/10/27 20:40:24] ( webml1 connect )
       OPERATION6:[21/10/27 20:39:07] ( TEACH->REPEAT )
       OPERATION7:[21/10/27 20:38:26] ( REPEAT->TEACH )
       OPERATION8:[21/10/27 20:38:08] ( TEACH->REPEAT )
       OPERATION9:[21/10/27 20:38:06] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 131 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

------------------------------------------------------------------------------
  46 - [21/10/27 20:41:12 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (E1432)[MC1]Communication error for PN voltage sensor(Servo board 1)
       OPERATION1:[21/10/27 20:41:12] ( ALLERESET )
       OPERATION2:[21/10/27 20:41:11] ( ALLERESET )
       OPERATION3:[21/10/27 20:40:28] ( EMERGENCY STOP )
       OPERATION4:[21/10/27 20:40:26] ( Control Power ON )
       OPERATION5:[21/10/27 20:40:24] ( webml1 connect )
       OPERATION6:[21/10/27 20:39:07] ( TEACH->REPEAT )
       OPERATION7:[21/10/27 20:38:26] ( REPEAT->TEACH )
       OPERATION8:[21/10/27 20:38:08] ( TEACH->REPEAT )
       OPERATION9:[21/10/27 20:38:06] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 78 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

------------------------------------------------------------------------------
  47 - [21/10/27 20:41:11 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (E1432)[MC1]Communication error for PN voltage sensor(Servo board 1)
       OPERATION1:[21/10/27 20:41:11] ( ALLERESET )
       OPERATION2:[21/10/27 20:40:28] ( EMERGENCY STOP )
       OPERATION3:[21/10/27 20:40:26] ( Control Power ON )
       OPERATION4:[21/10/27 20:40:24] ( webml1 connect )
       OPERATION5:[21/10/27 20:39:07] ( TEACH->REPEAT )
       OPERATION6:[21/10/27 20:38:26] ( REPEAT->TEACH )
       OPERATION7:[21/10/27 20:38:08] ( TEACH->REPEAT )
       OPERATION8:[21/10/27 20:38:06] ( EMERGENCY STOP )
       OPERATION9:[21/10/27 20:10:59] ( Motor power ON )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

------------------------------------------------------------------------------
  48 - [21/10/27 20:40:28 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (E1432)[MC1]Communication error for PN voltage sensor(Servo board 1)
       OPERATION1:[21/10/27 20:40:26] ( Control Power ON )
       OPERATION2:[21/10/27 20:40:24] ( webml1 connect )
       OPERATION3:[21/10/27 20:39:07] ( TEACH->REPEAT )
       OPERATION4:[21/10/27 20:38:26] ( REPEAT->TEACH )
       OPERATION5:[21/10/27 20:38:08] ( TEACH->REPEAT )
       OPERATION6:[21/10/27 20:38:06] ( EMERGENCY STOP )
       OPERATION7:[21/10/27 20:10:59] ( Motor power ON )
       OPERATION8:[21/10/27 19:48:21] ( EMERGENCY STOP )
       OPERATION9:[21/10/27 19:47:55] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

------------------------------------------------------------------------------
  49 - [21/10/27 20:39:50 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/27 20:39:07] ( TEACH->REPEAT )
       OPERATION2:[21/10/27 20:38:26] ( REPEAT->TEACH )
       OPERATION3:[21/10/27 20:38:08] ( TEACH->REPEAT )
       OPERATION4:[21/10/27 20:38:06] ( EMERGENCY STOP )
       OPERATION5:[21/10/27 20:10:59] ( Motor power ON )
       OPERATION6:[21/10/27 19:48:21] ( EMERGENCY STOP )
       OPERATION7:[21/10/27 19:47:55] ( ALLERESET )
       OPERATION8:[21/10/27 19:47:28] ( ALLERESET )
       OPERATION9:[21/10/27 19:47:23] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 224 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.161    14.933   -51.560    72.468     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4044.900

------------------------------------------------------------------------------
  50 - [21/10/27 19:45:31 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/27 19:45:29] ( Control Power ON )
       OPERATION2:[21/10/27 19:45:27] ( webml1 connect )
       OPERATION3:[21/10/27 18:30:04] ( EMERGENCY STOP )
       OPERATION4:[21/10/27 18:29:59] ( ALLERESET )
       OPERATION5:[21/10/27 18:29:42] ( ALLERESET )
       OPERATION6:[21/10/27 18:29:40] ( Motor power ON )
       OPERATION7:[21/10/27 18:29:37] ( password = 1234 )
       OPERATION8:[21/10/27 18:28:07] ( REPEAT->TEACH )
       OPERATION9:[21/10/27 17:15:31] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 226 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         41.416    49.530   -37.483   468.034     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       3890.100

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         41.416    49.530   -37.483   468.034     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       3890.100

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         41.416    49.530   -37.483   468.034     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       3890.100

------------------------------------------------------------------------------
  51 - [21/10/27 19:21:06 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/27 18:30:04] ( EMERGENCY STOP )
       OPERATION2:[21/10/27 18:29:59] ( ALLERESET )
       OPERATION3:[21/10/27 18:29:42] ( ALLERESET )
       OPERATION4:[21/10/27 18:29:40] ( Motor power ON )
       OPERATION5:[21/10/27 18:29:37] ( password = 1234 )
       OPERATION6:[21/10/27 18:28:07] ( REPEAT->TEACH )
       OPERATION7:[21/10/27 17:15:31] ( EMERGENCY STOP )
       OPERATION8:[21/10/27 16:23:18] ( Motor power ON )
       OPERATION9:[21/10/27 15:37:03] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 226 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         41.418    49.530   -37.483   467.248     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       3890.100

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         41.418    49.530   -37.483   467.248     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       3890.100

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         41.418    49.530   -37.483   467.248     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       3890.100

------------------------------------------------------------------------------
  52 - [21/10/27 18:29:57 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/27 18:29:42] ( ALLERESET )
       OPERATION2:[21/10/27 18:29:40] ( Motor power ON )
       OPERATION3:[21/10/27 18:29:37] ( password = 1234 )
       OPERATION4:[21/10/27 18:28:07] ( REPEAT->TEACH )
       OPERATION5:[21/10/27 17:15:31] ( EMERGENCY STOP )
       OPERATION6:[21/10/27 16:23:18] ( Motor power ON )
       OPERATION7:[21/10/27 15:37:03] ( EMERGENCY STOP )
       OPERATION8:[21/10/27 15:37:01] ( Control Power ON )
       OPERATION9:[21/10/27 15:36:59] ( webml1 connect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 226 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         40.609    48.312   -37.701    90.019     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       3890.100

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         41.416    49.543   -37.490    90.019     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       3890.100

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         40.609    48.312   -37.701    90.019     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       3890.100

------------------------------------------------------------------------------
  53 - [21/10/27 00:39:42 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/26 21:11:02] ( EMERGENCY STOP )
       OPERATION2:[21/10/26 21:05:24] ( EMERGENCY STOP )
       OPERATION3:[21/10/26 16:38:37] ( khidl1 disconnect )
       OPERATION4:[21/10/26 16:38:30] ( LOAD using.rcc )
       OPERATION5:[21/10/26 16:37:52] ( SAVE using.rcc )
       OPERATION6:[21/10/26 16:37:48] ( ID )
       OPERATION7:[21/10/26 16:37:48] ( TYPE DEXT(HERE,9) )
       OPERATION8:[21/10/26 16:37:48] ( TYPE DEXT(HERE,8) )
       OPERATION9:[21/10/26 16:37:48] ( TYPE DEXT(HERE,7) )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 224 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.470    -9.892    90.019     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4956.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.470    -9.892    90.019     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4956.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.470    -9.892    90.019     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       4956.000

------------------------------------------------------------------------------
  54 - [21/10/25 23:51:16 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/25 23:51:01] ( EMERGENCY STOP )
       OPERATION2:[21/10/25 23:12:45] ( AUX1 disconnect )
       OPERATION3:[21/10/25 23:12:19] ( save/full Hologic_ )
       OPERATION4:[21/10/25 23:11:39] ( AUX1 connect )
       OPERATION5:[21/10/25 22:11:26] ( EMERGENCY STOP )
       OPERATION6:[21/10/25 21:43:23] ( khidl1 disconnect )
       OPERATION7:[21/10/25 21:40:48] ( LOAD using.rcc )
       OPERATION8:[21/10/25 21:39:46] ( SAVE using.rcc )
       OPERATION9:[21/10/25 21:39:43] ( ID )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:147 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 107 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.019     0.000     0.000 63626.762

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      15082.501

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    90.019     0.000     0.000 63626.762

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      15082.501

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  55 - [21/10/25 20:45:38 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 20:41:51] ( EMERGENCY STOP )
       OPERATION2:[21/10/25 20:41:48] ( Control Power ON )
       OPERATION3:[21/10/25 20:41:47] ( webml1 connect )
       OPERATION4:[21/10/25 20:41:11] ( khidl1 disconnect )
       OPERATION5:[21/10/25 20:41:01] ( LOAD using.rcc )
       OPERATION6:[21/10/25 20:40:51] ( ALLERESET )
       OPERATION7:[21/10/25 20:31:06] ( LOAD using.rcc )
       OPERATION8:[21/10/25 20:28:17] ( ALLERESET )
       OPERATION9:[21/10/25 20:19:15] ( vis_measure_no = 3 )
       ROBOT1:
        PROGRAM:main Step:202 Cur_Step:206 STATUS:RUN
        Interpolation Type:LINEAR  S_parameter:0.095688
       PC1 PROGRAM: string_window Step No: 63 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -2.882   -15.734    66.094   153.241     0.000     0.000  1618.972

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        985.071

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -2.826   -15.695    66.173   153.237     0.000     0.000  1618.972

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        985.071

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -28.232    -9.989    38.746   150.405     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

------------------------------------------------------------------------------
  56 - [21/10/25 20:41:14 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/25 20:41:11] ( khidl1 disconnect )
       OPERATION2:[21/10/25 20:41:01] ( LOAD using.rcc )
       OPERATION3:[21/10/25 20:40:51] ( ALLERESET )
       OPERATION4:[21/10/25 20:31:06] ( LOAD using.rcc )
       OPERATION5:[21/10/25 20:28:17] ( ALLERESET )
       OPERATION6:[21/10/25 20:19:15] ( vis_measure_no = 3 )
       OPERATION7:[21/10/25 20:13:20] ( LOAD using.rcc )
       OPERATION8:[21/10/25 20:12:59] ( SAVE using.rcc )
       OPERATION9:[21/10/25 20:12:55] ( ID )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:147 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 114 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.476    -9.892    89.998     0.000     0.000  3251.763

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1386.771

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000  3251.763

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1386.771

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.482    -9.890    90.000     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  57 - [21/10/25 20:40:42 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 20:31:06] ( LOAD using.rcc )
       OPERATION2:[21/10/25 20:28:17] ( ALLERESET )
       OPERATION3:[21/10/25 20:19:15] ( vis_measure_no = 3 )
       OPERATION4:[21/10/25 20:13:20] ( LOAD using.rcc )
       OPERATION5:[21/10/25 20:12:59] ( SAVE using.rcc )
       OPERATION6:[21/10/25 20:12:55] ( ID )
       OPERATION7:[21/10/25 20:12:55] ( TYPE DEXT(HERE,9) )
       OPERATION8:[21/10/25 20:12:55] ( TYPE DEXT(HERE,8) )
       OPERATION9:[21/10/25 20:12:55] ( TYPE DEXT(HERE,7) )
       ROBOT1:
        PROGRAM:main Step:206 Cur_Step:207 STATUS:RUN
        Interpolation Type:LINLIN  S_parameter:0.929795
       PC1 PROGRAM: string_window Step No: 1 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -4.205   -10.541    69.312    72.514     0.000     0.000  1620.012

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        859.671

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -4.199   -10.538    69.320    72.523     0.000     0.000  1620.012

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        859.671

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -21.060    -3.276    41.899    70.905     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

------------------------------------------------------------------------------
  58 - [21/10/25 20:28:13 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 20:19:15] ( vis_measure_no = 3 )
       OPERATION2:[21/10/25 20:13:20] ( LOAD using.rcc )
       OPERATION3:[21/10/25 20:12:59] ( SAVE using.rcc )
       OPERATION4:[21/10/25 20:12:55] ( ID )
       OPERATION5:[21/10/25 20:12:55] ( TYPE DEXT(HERE,9) )
       OPERATION6:[21/10/25 20:12:55] ( TYPE DEXT(HERE,8) )
       OPERATION7:[21/10/25 20:12:55] ( TYPE DEXT(HERE,7) )
       OPERATION8:[21/10/25 20:12:55] ( TYPE SYSDATA (Lang )
       OPERATION9:[21/10/25 20:12:55] ( MESSAGES ON )
       ROBOT1:
        PROGRAM:main Step:378 Cur_Step:382 STATUS:RUN
        Interpolation Type:LINEAR  S_parameter:0.294584
       PC1 PROGRAM: string_window Step No: 114 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup Step No: 106 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         24.633   -36.285    54.087   154.526     0.000     0.000  1061.533

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1302.386

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         25.330   -36.171    54.727   154.776     0.000     0.000  1061.533

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       1302.386

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         18.733   -42.004     8.530   180.003     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  59 - [21/10/25 20:03:04 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 20:03:02] ( ALLERESET )
       OPERATION2:[21/10/25 20:03:00] ( ALLERESET )
       OPERATION3:[21/10/25 20:02:51] ( ALLERESET )
       OPERATION4:[21/10/25 20:00:30] ( ALLERESET )
       OPERATION5:[21/10/25 20:00:27] ( ALLERESET )
       OPERATION6:[21/10/25 20:00:23] ( ALLERESET )
       OPERATION7:[21/10/25 20:00:15] ( ALLERESET )
       OPERATION8:[21/10/25 19:51:32] ( ALLERESET )
       OPERATION9:[21/10/25 19:51:01] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 118 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.619     8.260    59.005    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.642     8.616    59.247    90.237     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.619     8.260    59.005    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  60 - [21/10/25 20:03:00 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 20:03:00] ( ALLERESET )
       OPERATION2:[21/10/25 20:02:51] ( ALLERESET )
       OPERATION3:[21/10/25 20:00:30] ( ALLERESET )
       OPERATION4:[21/10/25 20:00:27] ( ALLERESET )
       OPERATION5:[21/10/25 20:00:23] ( ALLERESET )
       OPERATION6:[21/10/25 20:00:15] ( ALLERESET )
       OPERATION7:[21/10/25 19:51:32] ( ALLERESET )
       OPERATION8:[21/10/25 19:51:01] ( ALLERESET )
       OPERATION9:[21/10/25 19:50:58] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 101 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.525     7.001    58.164    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.559     7.335    58.399    90.237     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.525     7.001    58.164    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  61 - [21/10/25 20:02:58 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 20:02:51] ( ALLERESET )
       OPERATION2:[21/10/25 20:00:30] ( ALLERESET )
       OPERATION3:[21/10/25 20:00:27] ( ALLERESET )
       OPERATION4:[21/10/25 20:00:23] ( ALLERESET )
       OPERATION5:[21/10/25 20:00:15] ( ALLERESET )
       OPERATION6:[21/10/25 19:51:32] ( ALLERESET )
       OPERATION7:[21/10/25 19:51:01] ( ALLERESET )
       OPERATION8:[21/10/25 19:50:58] ( ALLERESET )
       OPERATION9:[21/10/25 19:50:54] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.418     5.641    57.267    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.490     6.429    57.798    90.237     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.418     5.641    57.267    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  62 - [21/10/25 20:02:49 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 20:00:30] ( ALLERESET )
       OPERATION2:[21/10/25 20:00:27] ( ALLERESET )
       OPERATION3:[21/10/25 20:00:23] ( ALLERESET )
       OPERATION4:[21/10/25 20:00:15] ( ALLERESET )
       OPERATION5:[21/10/25 19:51:32] ( ALLERESET )
       OPERATION6:[21/10/25 19:51:01] ( ALLERESET )
       OPERATION7:[21/10/25 19:50:58] ( ALLERESET )
       OPERATION8:[21/10/25 19:50:54] ( ALLERESET )
       OPERATION9:[21/10/25 19:50:48] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 108 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.258     3.959    56.152    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.362     5.003    56.849    90.237     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -24.258     3.959    56.152    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  63 - [21/10/25 20:00:29 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 20:00:27] ( ALLERESET )
       OPERATION2:[21/10/25 20:00:23] ( ALLERESET )
       OPERATION3:[21/10/25 20:00:15] ( ALLERESET )
       OPERATION4:[21/10/25 19:51:32] ( ALLERESET )
       OPERATION5:[21/10/25 19:51:01] ( ALLERESET )
       OPERATION6:[21/10/25 19:50:58] ( ALLERESET )
       OPERATION7:[21/10/25 19:50:54] ( ALLERESET )
       OPERATION8:[21/10/25 19:50:48] ( ALLERESET )
       OPERATION9:[21/10/25 19:50:37] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 101 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.429    11.845    62.189    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.479    12.373    62.551    90.237     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.429    11.845    62.189    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  64 - [21/10/25 20:00:25 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 20:00:23] ( ALLERESET )
       OPERATION2:[21/10/25 20:00:15] ( ALLERESET )
       OPERATION3:[21/10/25 19:51:32] ( ALLERESET )
       OPERATION4:[21/10/25 19:51:01] ( ALLERESET )
       OPERATION5:[21/10/25 19:50:58] ( ALLERESET )
       OPERATION6:[21/10/25 19:50:54] ( ALLERESET )
       OPERATION7:[21/10/25 19:50:48] ( ALLERESET )
       OPERATION8:[21/10/25 19:50:37] ( ALLERESET )
       OPERATION9:[21/10/25 19:50:27] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.347    10.900    61.559    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.383    11.268    61.814    90.237     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.347    10.900    61.559    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  65 - [21/10/25 20:00:19 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 20:00:15] ( ALLERESET )
       OPERATION2:[21/10/25 19:51:32] ( ALLERESET )
       OPERATION3:[21/10/25 19:51:01] ( ALLERESET )
       OPERATION4:[21/10/25 19:50:58] ( ALLERESET )
       OPERATION5:[21/10/25 19:50:54] ( ALLERESET )
       OPERATION6:[21/10/25 19:50:48] ( ALLERESET )
       OPERATION7:[21/10/25 19:50:37] ( ALLERESET )
       OPERATION8:[21/10/25 19:50:27] ( ALLERESET )
       OPERATION9:[21/10/25 19:32:37] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.219     9.692    60.752    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.278    10.220    61.115    90.237     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.219     9.692    60.752    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  66 - [21/10/25 20:00:13 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 19:51:32] ( ALLERESET )
       OPERATION2:[21/10/25 19:51:01] ( ALLERESET )
       OPERATION3:[21/10/25 19:50:58] ( ALLERESET )
       OPERATION4:[21/10/25 19:50:54] ( ALLERESET )
       OPERATION5:[21/10/25 19:50:48] ( ALLERESET )
       OPERATION6:[21/10/25 19:50:37] ( ALLERESET )
       OPERATION7:[21/10/25 19:50:27] ( ALLERESET )
       OPERATION8:[21/10/25 19:32:37] ( ALLERESET )
       OPERATION9:[21/10/25 19:32:26] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: conv_control.pc Step No: 14 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.085     8.501    59.967    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.147     9.059    60.338    90.237     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -18.085     8.501    59.967    90.236     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  67 - [21/10/25 19:51:30 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 19:51:01] ( ALLERESET )
       OPERATION2:[21/10/25 19:50:58] ( ALLERESET )
       OPERATION3:[21/10/25 19:50:54] ( ALLERESET )
       OPERATION4:[21/10/25 19:50:48] ( ALLERESET )
       OPERATION5:[21/10/25 19:50:37] ( ALLERESET )
       OPERATION6:[21/10/25 19:50:27] ( ALLERESET )
       OPERATION7:[21/10/25 19:32:37] ( ALLERESET )
       OPERATION8:[21/10/25 19:32:26] ( ALLERESET )
       OPERATION9:[21/10/25 19:32:00] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 107 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -17.446    13.561    63.163    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -17.447    13.568    63.175    90.237     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -17.446    13.561    63.163    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  68 - [21/10/25 19:50:59 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 19:50:58] ( ALLERESET )
       OPERATION2:[21/10/25 19:50:54] ( ALLERESET )
       OPERATION3:[21/10/25 19:50:48] ( ALLERESET )
       OPERATION4:[21/10/25 19:50:37] ( ALLERESET )
       OPERATION5:[21/10/25 19:50:27] ( ALLERESET )
       OPERATION6:[21/10/25 19:32:37] ( ALLERESET )
       OPERATION7:[21/10/25 19:32:26] ( ALLERESET )
       OPERATION8:[21/10/25 19:32:00] ( ALLERESET )
       OPERATION9:[21/10/25 17:56:48] ( khidl1 disconnect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -20.039    11.116    61.969    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -20.055    11.241    62.063    90.237     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -20.039    11.116    61.969    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  69 - [21/10/25 19:50:56 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 19:50:54] ( ALLERESET )
       OPERATION2:[21/10/25 19:50:48] ( ALLERESET )
       OPERATION3:[21/10/25 19:50:37] ( ALLERESET )
       OPERATION4:[21/10/25 19:50:27] ( ALLERESET )
       OPERATION5:[21/10/25 19:32:37] ( ALLERESET )
       OPERATION6:[21/10/25 19:32:26] ( ALLERESET )
       OPERATION7:[21/10/25 19:32:00] ( ALLERESET )
       OPERATION8:[21/10/25 17:56:48] ( khidl1 disconnect )
       OPERATION9:[21/10/25 17:56:01] ( LOAD using.rcc )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 101 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -20.025    10.864    61.809    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -20.033    10.953    61.872    90.237     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -20.025    10.864    61.809    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  70 - [21/10/25 19:50:53 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 19:50:48] ( ALLERESET )
       OPERATION2:[21/10/25 19:50:37] ( ALLERESET )
       OPERATION3:[21/10/25 19:50:27] ( ALLERESET )
       OPERATION4:[21/10/25 19:32:37] ( ALLERESET )
       OPERATION5:[21/10/25 19:32:26] ( ALLERESET )
       OPERATION6:[21/10/25 19:32:00] ( ALLERESET )
       OPERATION7:[21/10/25 17:56:48] ( khidl1 disconnect )
       OPERATION8:[21/10/25 17:56:01] ( LOAD using.rcc )
       OPERATION9:[21/10/25 17:54:53] ( SAVE using.rcc )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -19.999    10.589    61.625    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -20.008    10.637    61.663    90.237     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -19.999    10.589    61.625    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  71 - [21/10/25 19:50:44 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 19:50:37] ( ALLERESET )
       OPERATION2:[21/10/25 19:50:27] ( ALLERESET )
       OPERATION3:[21/10/25 19:32:37] ( ALLERESET )
       OPERATION4:[21/10/25 19:32:26] ( ALLERESET )
       OPERATION5:[21/10/25 19:32:00] ( ALLERESET )
       OPERATION6:[21/10/25 17:56:48] ( khidl1 disconnect )
       OPERATION7:[21/10/25 17:56:01] ( LOAD using.rcc )
       OPERATION8:[21/10/25 17:54:53] ( SAVE using.rcc )
       OPERATION9:[21/10/25 17:53:37] ( SAVE using.rcc )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -19.965    10.167    61.352    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -19.969    10.165    61.351    90.237     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -19.965    10.167    61.352    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  72 - [21/10/25 19:50:35 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 19:50:27] ( ALLERESET )
       OPERATION2:[21/10/25 19:32:37] ( ALLERESET )
       OPERATION3:[21/10/25 19:32:26] ( ALLERESET )
       OPERATION4:[21/10/25 19:32:00] ( ALLERESET )
       OPERATION5:[21/10/25 17:56:48] ( khidl1 disconnect )
       OPERATION6:[21/10/25 17:56:01] ( LOAD using.rcc )
       OPERATION7:[21/10/25 17:54:53] ( SAVE using.rcc )
       OPERATION8:[21/10/25 17:53:37] ( SAVE using.rcc )
       OPERATION9:[21/10/25 17:53:19] ( SAVE using.rcc )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -19.809     8.627    60.311    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -19.957    10.035    61.264    90.237     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -19.809     8.627    60.311    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  73 - [21/10/25 19:50:24 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 19:32:37] ( ALLERESET )
       OPERATION2:[21/10/25 19:32:26] ( ALLERESET )
       OPERATION3:[21/10/25 19:32:00] ( ALLERESET )
       OPERATION4:[21/10/25 17:56:48] ( khidl1 disconnect )
       OPERATION5:[21/10/25 17:56:01] ( LOAD using.rcc )
       OPERATION6:[21/10/25 17:54:53] ( SAVE using.rcc )
       OPERATION7:[21/10/25 17:53:37] ( SAVE using.rcc )
       OPERATION8:[21/10/25 17:53:19] ( SAVE using.rcc )
       OPERATION9:[21/10/25 17:53:15] ( ID )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -19.632     7.003    59.240    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -19.792     8.337    60.137    90.237     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -19.632     7.003    59.240    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        266.700

------------------------------------------------------------------------------
  74 - [21/10/25 19:32:35 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 19:32:26] ( ALLERESET )
       OPERATION2:[21/10/25 19:32:00] ( ALLERESET )
       OPERATION3:[21/10/25 17:56:48] ( khidl1 disconnect )
       OPERATION4:[21/10/25 17:56:01] ( LOAD using.rcc )
       OPERATION5:[21/10/25 17:54:53] ( SAVE using.rcc )
       OPERATION6:[21/10/25 17:53:37] ( SAVE using.rcc )
       OPERATION7:[21/10/25 17:53:19] ( SAVE using.rcc )
       OPERATION8:[21/10/25 17:53:15] ( ID )
       OPERATION9:[21/10/25 17:53:15] ( TYPE DEXT(HERE,9) )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 101 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -25.772     8.318    51.718    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       7710.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -25.771     8.314    51.714    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       7710.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -25.772     8.318    51.718    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       7710.000

------------------------------------------------------------------------------
  75 - [21/10/25 19:32:24 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 19:32:00] ( ALLERESET )
       OPERATION2:[21/10/25 17:56:48] ( khidl1 disconnect )
       OPERATION3:[21/10/25 17:56:01] ( LOAD using.rcc )
       OPERATION4:[21/10/25 17:54:53] ( SAVE using.rcc )
       OPERATION5:[21/10/25 17:53:37] ( SAVE using.rcc )
       OPERATION6:[21/10/25 17:53:19] ( SAVE using.rcc )
       OPERATION7:[21/10/25 17:53:15] ( ID )
       OPERATION8:[21/10/25 17:53:15] ( TYPE DEXT(HERE,9) )
       OPERATION9:[21/10/25 17:53:15] ( TYPE DEXT(HERE,8) )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -25.736     7.940    51.442    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       7710.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -25.760     8.188    51.625    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       7710.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -25.736     7.940    51.442    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       7710.000

------------------------------------------------------------------------------
  76 - [21/10/25 19:31:50 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 17:56:48] ( khidl1 disconnect )
       OPERATION2:[21/10/25 17:56:01] ( LOAD using.rcc )
       OPERATION3:[21/10/25 17:54:53] ( SAVE using.rcc )
       OPERATION4:[21/10/25 17:53:37] ( SAVE using.rcc )
       OPERATION5:[21/10/25 17:53:19] ( SAVE using.rcc )
       OPERATION6:[21/10/25 17:53:15] ( ID )
       OPERATION7:[21/10/25 17:53:15] ( TYPE DEXT(HERE,9) )
       OPERATION8:[21/10/25 17:53:15] ( TYPE DEXT(HERE,8) )
       OPERATION9:[21/10/25 17:53:15] ( TYPE DEXT(HERE,7) )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -25.664     7.213    50.933    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       7710.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -25.760     8.188    51.625    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       7710.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -25.664     7.213    50.933    90.236     0.000     0.000  1126.233

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       7710.000

------------------------------------------------------------------------------
  77 - [21/10/25 16:58:09 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 16:58:02] ( Motor power ON )
       OPERATION2:[21/10/25 16:49:24] ( ALLERESET )
       OPERATION3:[21/10/25 16:45:22] ( Motor power ON )
       OPERATION4:[21/10/25 16:29:07] ( EMERGENCY STOP )
       OPERATION5:[21/10/25 16:29:07] ( ALLERESET )
       OPERATION6:[21/10/25 16:28:44] ( Motor power ON )
       OPERATION7:[21/10/25 16:28:36] ( password = 1234 )
       OPERATION8:[21/10/25 13:59:41] ( as )
       OPERATION9:[21/10/25 13:59:16] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -23.425    37.208    -3.008    90.226     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -23.351    37.240    -2.945    90.228     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -23.425    37.208    -3.008    90.226     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  78 - [21/10/25 16:49:21 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 16:45:22] ( Motor power ON )
       OPERATION2:[21/10/25 16:29:07] ( EMERGENCY STOP )
       OPERATION3:[21/10/25 16:29:07] ( ALLERESET )
       OPERATION4:[21/10/25 16:28:44] ( Motor power ON )
       OPERATION5:[21/10/25 16:28:36] ( password = 1234 )
       OPERATION6:[21/10/25 13:59:41] ( as )
       OPERATION7:[21/10/25 13:59:16] ( EMERGENCY STOP )
       OPERATION8:[21/10/25 13:59:13] ( Control Power ON )
       OPERATION9:[21/10/25 13:59:12] ( webml1 connect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 114 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -31.447    33.247   -10.381    90.077     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -30.862    33.486    -9.888    90.079     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -31.447    33.247   -10.381    90.077     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  79 - [21/10/25 16:29:03 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/25 16:28:44] ( Motor power ON )
       OPERATION2:[21/10/25 16:28:36] ( password = 1234 )
       OPERATION3:[21/10/25 13:59:41] ( as )
       OPERATION4:[21/10/25 13:59:16] ( EMERGENCY STOP )
       OPERATION5:[21/10/25 13:59:13] ( Control Power ON )
       OPERATION6:[21/10/25 13:59:12] ( webml1 connect )
       OPERATION7:[21/10/22 16:41:23] ( cvreset 8 )
       OPERATION8:[21/10/22 16:41:18] ( cvreset 7 )
       OPERATION9:[21/10/22 16:27:21] ( khidl1 disconnect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -51.228     8.330    21.791     7.464     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -51.204     8.890    22.286     7.467     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -51.228     8.330    21.791     7.464     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  80 - [21/10/23 00:08:40 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/22 16:41:23] ( cvreset 8 )
       OPERATION2:[21/10/22 16:41:18] ( cvreset 7 )
       OPERATION3:[21/10/22 16:27:21] ( khidl1 disconnect )
       OPERATION4:[21/10/22 16:27:00] ( SAVE using.rcc )
       OPERATION5:[21/10/22 16:26:45] ( SAVE using.rcc )
       OPERATION6:[21/10/22 16:26:42] ( ID )
       OPERATION7:[21/10/22 16:26:42] ( TYPE DEXT(HERE,9) )
       OPERATION8:[21/10/22 16:26:42] ( TYPE DEXT(HERE,8) )
       OPERATION9:[21/10/22 16:26:42] ( TYPE DEXT(HERE,7) )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -29.751   -29.018   -12.720     7.469     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       6549.900

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -29.756   -29.017   -12.722     7.471     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       6549.900

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -29.751   -29.018   -12.720     7.469     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       6549.900

------------------------------------------------------------------------------
  81 - [21/10/22 16:24:55 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/22 16:24:51] ( Motor power ON )
       OPERATION2:[21/10/22 16:17:48] ( REPEAT->TEACH )
       OPERATION3:[21/10/22 16:16:50] ( password = 1234 )
       OPERATION4:[21/10/22 16:16:46] ( ALLERESET )
       OPERATION5:[21/10/22 16:08:18] ( EMERGENCY STOP )
       OPERATION6:[21/10/22 16:08:16] ( Control Power ON )
       OPERATION7:[21/10/22 16:08:14] ( webml1 connect )
       OPERATION8:[21/10/22 16:05:33] ( cvreset 7 )
       OPERATION9:[21/10/22 16:00:46] ( cvreset 8 )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -37.182    -0.204    22.884     7.469     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -36.512     0.279    23.214     7.471     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -37.182    -0.204    22.884     7.469     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  82 - [21/10/22 16:08:18 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/22 16:08:16] ( Control Power ON )
       OPERATION2:[21/10/22 16:08:14] ( webml1 connect )
       OPERATION3:[21/10/22 16:05:33] ( cvreset 7 )
       OPERATION4:[21/10/22 16:00:46] ( cvreset 8 )
       OPERATION5:[21/10/22 16:00:33] ( cvreset 7 )
       OPERATION6:[21/10/22 16:00:07] ( ALLERESET )
       OPERATION7:[21/10/22 15:59:35] ( as )
       OPERATION8:[21/10/22 15:59:10] ( EMERGENCY STOP )
       OPERATION9:[21/10/22 15:59:07] ( Control Power ON )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 53 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000   960.068

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000   960.068

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000   960.068

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

------------------------------------------------------------------------------
  83 - [21/10/22 16:07:44 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/22 16:05:33] ( cvreset 7 )
       OPERATION2:[21/10/22 16:00:46] ( cvreset 8 )
       OPERATION3:[21/10/22 16:00:33] ( cvreset 7 )
       OPERATION4:[21/10/22 16:00:07] ( ALLERESET )
       OPERATION5:[21/10/22 15:59:35] ( as )
       OPERATION6:[21/10/22 15:59:10] ( EMERGENCY STOP )
       OPERATION7:[21/10/22 15:59:07] ( Control Power ON )
       OPERATION8:[21/10/22 15:59:06] ( webml1 connect )
       OPERATION9:[21/10/22 15:58:08] ( password = 1234 )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 53 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000   960.068

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000   960.068

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000   960.068

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

------------------------------------------------------------------------------
  84 - [21/10/22 15:59:10 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/22 15:59:07] ( Control Power ON )
       OPERATION2:[21/10/22 15:59:06] ( webml1 connect )
       OPERATION3:[21/10/22 15:58:08] ( password = 1234 )
       OPERATION4:[21/10/22 15:57:52] ( ALLERESET )
       OPERATION5:[21/10/22 14:33:42] ( as )
       OPERATION6:[21/10/22 14:33:17] ( EMERGENCY STOP )
       OPERATION7:[21/10/22 14:33:14] ( Control Power ON )
       OPERATION8:[21/10/22 14:33:13] ( webml1 connect )
       OPERATION9:[21/10/22 00:12:23] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 53 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25817.701

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25817.701

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25817.701

------------------------------------------------------------------------------
  85 - [21/10/22 15:58:35 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/22 15:58:08] ( password = 1234 )
       OPERATION2:[21/10/22 15:57:52] ( ALLERESET )
       OPERATION3:[21/10/22 14:33:42] ( as )
       OPERATION4:[21/10/22 14:33:17] ( EMERGENCY STOP )
       OPERATION5:[21/10/22 14:33:14] ( Control Power ON )
       OPERATION6:[21/10/22 14:33:13] ( webml1 connect )
       OPERATION7:[21/10/22 00:12:23] ( EMERGENCY STOP )
       OPERATION8:[21/10/22 00:09:11] ( cvreset 7 )
       OPERATION9:[21/10/22 00:08:15] ( cvreset 7 )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 53 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25817.701

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25817.701

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      25817.701

------------------------------------------------------------------------------
  86 - [21/10/22 14:33:17 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/22 14:33:14] ( Control Power ON )
       OPERATION2:[21/10/22 14:33:13] ( webml1 connect )
       OPERATION3:[21/10/22 00:12:23] ( EMERGENCY STOP )
       OPERATION4:[21/10/22 00:09:11] ( cvreset 7 )
       OPERATION5:[21/10/22 00:08:15] ( cvreset 7 )
       OPERATION6:[21/10/22 00:06:34] ( cvreset 8 )
       OPERATION7:[21/10/22 00:06:26] ( cvreset 7 )
       OPERATION8:[21/10/21 23:03:01] ( password = 1234 )
       OPERATION9:[21/10/21 23:02:53] ( ALLERESET )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000    37.619

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

         99.600

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000    37.619

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

         99.600

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.201    22.206     7.471     0.000     0.000    37.619

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

         99.600

------------------------------------------------------------------------------
  87 - [21/10/22 00:13:38 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/22 00:12:23] ( EMERGENCY STOP )
       OPERATION2:[21/10/22 00:09:11] ( cvreset 7 )
       OPERATION3:[21/10/22 00:08:15] ( cvreset 7 )
       OPERATION4:[21/10/22 00:06:34] ( cvreset 8 )
       OPERATION5:[21/10/22 00:06:26] ( cvreset 7 )
       OPERATION6:[21/10/21 23:03:01] ( password = 1234 )
       OPERATION7:[21/10/21 23:02:53] ( ALLERESET )
       OPERATION8:[21/10/21 22:23:33] ( EMERGENCY STOP )
       OPERATION9:[21/10/21 22:23:31] ( Control Power ON )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.203    22.206     7.473     0.000     0.000    37.619

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

         99.600

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.203    22.206     7.473     0.000     0.000    37.619

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

         99.600

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.203    22.206     7.473     0.000     0.000    37.619

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

         99.600

------------------------------------------------------------------------------
  88 - [21/10/21 22:23:33 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/21 22:23:31] ( Control Power ON )
       OPERATION2:[21/10/21 22:23:29] ( webml1 connect )
       OPERATION3:[21/10/21 22:03:20] ( ALLERESET )
       OPERATION4:[21/10/21 22:01:21] ( khidl1 disconnect )
       OPERATION5:[21/10/21 21:59:18] ( SAVE using.rcc )
       OPERATION6:[21/10/21 21:59:09] ( LOAD using.rcc )
       OPERATION7:[21/10/21 21:58:26] ( SAVE using.rcc )
       OPERATION8:[21/10/21 21:58:23] ( ID )
       OPERATION9:[21/10/21 21:58:22] ( TYPE DEXT(HERE,9) )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:206 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.203    22.206     7.473     0.000     0.000 65625.516

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.203    22.206     7.473     0.000     0.000 65625.516

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -8.608    68.989    -4.901    75.966     0.000     0.000   864.611

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        725.554

------------------------------------------------------------------------------
  89 - [21/10/21 22:22:56 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/21 22:03:20] ( ALLERESET )
       OPERATION2:[21/10/21 22:01:21] ( khidl1 disconnect )
       OPERATION3:[21/10/21 21:59:18] ( SAVE using.rcc )
       OPERATION4:[21/10/21 21:59:09] ( LOAD using.rcc )
       OPERATION5:[21/10/21 21:58:26] ( SAVE using.rcc )
       OPERATION6:[21/10/21 21:58:23] ( ID )
       OPERATION7:[21/10/21 21:58:22] ( TYPE DEXT(HERE,9) )
       OPERATION8:[21/10/21 21:58:22] ( TYPE DEXT(HERE,8) )
       OPERATION9:[21/10/21 21:58:22] ( TYPE DEXT(HERE,7) )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:206 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.566    -1.203    22.206     7.473     0.000     0.000 65625.516

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.567    -1.207    22.210     7.550     0.000     0.000 65625.516

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -8.608    68.989    -4.901    75.966     0.000     0.000   864.611

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        725.554

------------------------------------------------------------------------------
  90 - [21/10/21 22:02:43 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/21 22:01:21] ( khidl1 disconnect )
       OPERATION2:[21/10/21 21:59:18] ( SAVE using.rcc )
       OPERATION3:[21/10/21 21:59:09] ( LOAD using.rcc )
       OPERATION4:[21/10/21 21:58:26] ( SAVE using.rcc )
       OPERATION5:[21/10/21 21:58:23] ( ID )
       OPERATION6:[21/10/21 21:58:22] ( TYPE DEXT(HERE,9) )
       OPERATION7:[21/10/21 21:58:22] ( TYPE DEXT(HERE,8) )
       OPERATION8:[21/10/21 21:58:22] ( TYPE DEXT(HERE,7) )
       OPERATION9:[21/10/21 21:58:22] ( TYPE SYSDATA (Lang )
       ROBOT1:
        PROGRAM:main Step:202 Cur_Step:206 STATUS:RUN
        Interpolation Type:LINEAR  S_parameter:0.680891
       PC1 PROGRAM: string_window Step No: 72 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.688    -0.985    21.823     7.462     0.000     0.000   989.266

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -38.567    -1.207    22.210     7.550     0.000     0.000   989.266

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         -8.608    68.989    -4.901    75.966     0.000     0.000   864.611

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        725.554

------------------------------------------------------------------------------
  91 - [21/10/21 21:49:40 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/21 21:47:34] ( Motor power ON )
       OPERATION2:[21/10/21 21:47:29] ( password = 1234 )
       OPERATION3:[21/10/21 21:46:30] ( Control Power ON )
       OPERATION4:[21/10/21 21:46:29] ( webml1 connect )
       OPERATION5:[21/10/21 21:05:20] ( ERROR RESET )
       OPERATION6:[21/10/21 21:05:16] ( ERROR RESET )
       OPERATION7:[21/10/21 21:01:30] ( password = 1234 )
       OPERATION8:[21/10/21 21:00:20] ( as )
       OPERATION9:[21/10/21 20:59:55] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:146 Cur_Step:163 STATUS:WAIT
        Interpolation Type:LINEAR  S_parameter:0.000000
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.667     4.748     8.923    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.669     4.748     8.923    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.669     4.748     8.923    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      20281.201

------------------------------------------------------------------------------
  92 - [21/10/21 21:45:56 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/21 21:05:20] ( ERROR RESET )
       OPERATION2:[21/10/21 21:05:16] ( ERROR RESET )
       OPERATION3:[21/10/21 21:01:30] ( password = 1234 )
       OPERATION4:[21/10/21 21:00:20] ( as )
       OPERATION5:[21/10/21 20:59:55] ( EMERGENCY STOP )
       OPERATION6:[21/10/21 20:59:52] ( Control Power ON )
       OPERATION7:[21/10/21 20:59:51] ( webml1 connect )
       OPERATION8:[21/10/21 20:59:11] ( ERESET 1: )
       OPERATION9:[21/10/21 20:57:32] ( ERESET 1: )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 64 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.653     4.746     8.921    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.653     4.746     8.921    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.653     4.746     8.921    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  93 - [21/10/21 20:59:19 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/21 20:59:11] ( ERESET 1: )
       OPERATION2:[21/10/21 20:57:32] ( ERESET 1: )
       OPERATION3:[21/10/21 20:57:24] ( ERESET 1: )
       OPERATION4:[21/10/21 20:53:31] ( STP_ONCE OFF )
       OPERATION5:[21/10/21 20:53:26] ( STP_ONCE ON )
       OPERATION6:[21/10/21 20:53:11] ( REP_ONCE OFF )
       OPERATION7:[21/10/21 20:52:54] ( REP_ONCE ON )
       OPERATION8:[21/10/21 20:52:26] ( DIRECTORY/N/P/CMT  )
       OPERATION9:[21/10/21 20:52:22] ( DIRECTORY/N/P/CMT  )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.653     4.746     8.921    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        545.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.653     4.746     8.921    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        545.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.653     4.746     8.921    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        545.700

------------------------------------------------------------------------------
  94 - [21/10/21 20:44:03 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/21 20:36:27] ( CYCLE START )
       OPERATION2:[21/10/21 20:36:23] ( Motor power ON )
       OPERATION3:[21/10/21 20:34:06] ( vis_measure_no = 3 )
       OPERATION4:[21/10/21 20:33:26] ( password = 1234 )
       OPERATION5:[21/10/21 20:33:18] ( EMERGENCY STOP )
       OPERATION6:[21/10/21 20:33:15] ( Control Power ON )
       OPERATION7:[21/10/21 20:33:14] ( webml1 connect )
       OPERATION8:[21/10/21 20:32:19] ( ERESET 1: )
       OPERATION9:[21/10/21 20:32:08] ( ERESET 1: )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:147 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 66 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.653     4.746     8.921    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.669     4.748     8.923    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.669     4.748     8.923    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      20281.201

------------------------------------------------------------------------------
  95 - [21/10/21 20:32:41 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/21 20:32:19] ( ERESET 1: )
       OPERATION2:[21/10/21 20:32:08] ( ERESET 1: )
       OPERATION3:[21/10/21 20:31:08] ( ERESET 1: )
       OPERATION4:[21/10/21 20:31:05] ( password = 1234 )
       OPERATION5:[21/10/21 20:30:48] ( ERESET 1: )
       OPERATION6:[21/10/21 20:24:41] ( CYCLE START )
       OPERATION7:[21/10/21 20:24:38] ( Motor power ON )
       OPERATION8:[21/10/21 20:24:35] ( CYCLE START )
       OPERATION9:[21/10/21 20:22:07] ( PRIME/C 1:main,-1 )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: plc_control.pc Step No: 217 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.653     4.746     8.921    -0.207     0.000     0.000112511.164

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.653     4.746     8.921    -0.207     0.000     0.000112511.164

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -42.653     4.746     8.921    -0.207     0.000     0.000112511.164

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  96 - [21/10/21 19:36:44 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/21 19:34:57] ( as )
       OPERATION2:[21/10/21 19:34:57] ( password = 1234 )
       OPERATION3:[21/10/21 19:34:32] ( EMERGENCY STOP )
       OPERATION4:[21/10/21 19:34:30] ( Control Power ON )
       OPERATION5:[21/10/21 19:34:28] ( webml1 connect )
       OPERATION6:[21/10/21 19:33:07] ( TEACH->REPEAT )
       OPERATION7:[21/10/21 19:26:19] ( ERESET 1: )
       OPERATION8:[21/10/21 19:25:20] ( password = 1234 )
       OPERATION9:[21/10/21 19:24:02] ( SETHOME 2,HERE )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:22 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       PC4 PROGRAM: setup_select Step No: 27 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -28.045   -13.204   -18.721    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      20281.201

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -28.045   -13.204   -18.721    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      20281.201

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -28.045   -13.204   -18.721    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      20281.201

------------------------------------------------------------------------------
  97 - [21/10/21 19:33:55 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/21 19:33:07] ( TEACH->REPEAT )
       OPERATION2:[21/10/21 19:26:19] ( ERESET 1: )
       OPERATION3:[21/10/21 19:25:20] ( password = 1234 )
       OPERATION4:[21/10/21 19:24:02] ( SETHOME 2,HERE )
       OPERATION5:[21/10/21 19:23:46] ( SETHOME HERE )
       OPERATION6:[21/10/21 19:23:23] ( SAVE using.rcc )
       OPERATION7:[21/10/21 19:23:19] ( ID )
       OPERATION8:[21/10/21 19:23:19] ( TYPE DEXT(HERE,9) )
       OPERATION9:[21/10/21 19:23:19] ( TYPE DEXT(HERE,8) )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:22 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       PC4 PROGRAM: setup_select Step No: 27 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -28.045   -13.204   -18.721    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      20281.201

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -28.060   -13.202   -18.720    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      20281.201

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -28.045   -13.204   -18.721    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      20281.201

------------------------------------------------------------------------------
  98 - [21/10/21 19:11:05 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/21 19:10:44] ( ALLERESET )
       OPERATION2:[21/10/21 19:10:27] ( ALLERESET )
       OPERATION3:[21/10/21 19:10:19] ( ALLERESET )
       OPERATION4:[21/10/21 19:10:02] ( ALLERESET )
       OPERATION5:[21/10/21 19:09:56] ( Motor power ON )
       OPERATION6:[21/10/21 19:09:47] ( ALLERESET )
       OPERATION7:[21/10/21 19:09:37] ( ALLERESET )
       OPERATION8:[21/10/21 19:08:17] ( ALLERESET )
       OPERATION9:[21/10/21 19:08:06] ( ALLERESET )
       ROBOT1:
        PROGRAM:home Step:0 Cur_Step:31 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -32.178     5.391    48.394    -0.205     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      12014.101

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -31.089     6.237    48.792    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      12014.101

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

          0.000    -0.001     0.000    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
  99 - [21/10/21 19:10:37 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/21 19:10:27] ( ALLERESET )
       OPERATION2:[21/10/21 19:10:19] ( ALLERESET )
       OPERATION3:[21/10/21 19:10:02] ( ALLERESET )
       OPERATION4:[21/10/21 19:09:56] ( Motor power ON )
       OPERATION5:[21/10/21 19:09:47] ( ALLERESET )
       OPERATION6:[21/10/21 19:09:37] ( ALLERESET )
       OPERATION7:[21/10/21 19:08:17] ( ALLERESET )
       OPERATION8:[21/10/21 19:08:06] ( ALLERESET )
       OPERATION9:[21/10/21 18:59:12] ( here home1 )
       ROBOT1:
        PROGRAM:home Step:0 Cur_Step:31 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -17.668     1.456    69.376    -0.205     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      12014.101

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -17.734     2.086    69.744    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      12014.101

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

          0.000    -0.001     0.000    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
 100 - [21/10/21 19:10:25 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/21 19:10:19] ( ALLERESET )
       OPERATION2:[21/10/21 19:10:02] ( ALLERESET )
       OPERATION3:[21/10/21 19:09:56] ( Motor power ON )
       OPERATION4:[21/10/21 19:09:47] ( ALLERESET )
       OPERATION5:[21/10/21 19:09:37] ( ALLERESET )
       OPERATION6:[21/10/21 19:08:17] ( ALLERESET )
       OPERATION7:[21/10/21 19:08:06] ( ALLERESET )
       OPERATION8:[21/10/21 18:59:12] ( here home1 )
       OPERATION9:[21/10/21 18:57:32] ( Motor power ON )
       ROBOT1:
        PROGRAM:home Step:0 Cur_Step:31 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         41.319   -39.651    42.359    -0.205     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      12014.101

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         42.113   -39.591    42.430    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      12014.101

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

          0.000    -0.001     0.000    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
 101 - [21/10/21 19:10:17 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/21 19:10:02] ( ALLERESET )
       OPERATION2:[21/10/21 19:09:56] ( Motor power ON )
       OPERATION3:[21/10/21 19:09:47] ( ALLERESET )
       OPERATION4:[21/10/21 19:09:37] ( ALLERESET )
       OPERATION5:[21/10/21 19:08:17] ( ALLERESET )
       OPERATION6:[21/10/21 19:08:06] ( ALLERESET )
       OPERATION7:[21/10/21 18:59:12] ( here home1 )
       OPERATION8:[21/10/21 18:57:32] ( Motor power ON )
       OPERATION9:[21/10/21 18:56:53] ( REPEAT->TEACH )
       ROBOT1:
        PROGRAM:home Step:0 Cur_Step:31 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 107 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         23.980   -35.987    54.246    -0.205     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      12014.101

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         24.580   -35.820    54.997    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      12014.101

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

          0.000    -0.001     0.000    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
 102 - [21/10/21 19:09:59 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/21 19:09:56] ( Motor power ON )
       OPERATION2:[21/10/21 19:09:47] ( ALLERESET )
       OPERATION3:[21/10/21 19:09:37] ( ALLERESET )
       OPERATION4:[21/10/21 19:08:17] ( ALLERESET )
       OPERATION5:[21/10/21 19:08:06] ( ALLERESET )
       OPERATION6:[21/10/21 18:59:12] ( here home1 )
       OPERATION7:[21/10/21 18:57:32] ( Motor power ON )
       OPERATION8:[21/10/21 18:56:53] ( REPEAT->TEACH )
       OPERATION9:[21/10/21 18:55:57] ( SAVE using.rcc )
       ROBOT1:
        PROGRAM:home Step:0 Cur_Step:31 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 63 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 18 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.047    49.189   -37.128    -0.205     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      11610.087

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         20.436    49.712   -37.015    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      11610.087

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

          0.000    -0.001     0.000    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
 103 - [21/10/21 19:09:45 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/21 19:09:37] ( ALLERESET )
       OPERATION2:[21/10/21 19:08:17] ( ALLERESET )
       OPERATION3:[21/10/21 19:08:06] ( ALLERESET )
       OPERATION4:[21/10/21 18:59:12] ( here home1 )
       OPERATION5:[21/10/21 18:57:32] ( Motor power ON )
       OPERATION6:[21/10/21 18:56:53] ( REPEAT->TEACH )
       OPERATION7:[21/10/21 18:55:57] ( SAVE using.rcc )
       OPERATION8:[21/10/21 18:55:44] ( SAVE using.rcc )
       OPERATION9:[21/10/21 18:55:40] ( ID )
       ROBOT1:
        PROGRAM:home Step:0 Cur_Step:31 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         19.390    48.318   -37.300    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      10413.301

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         19.390    48.318   -37.300    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      10413.301

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

          0.000    -0.001     0.000    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
 104 - [21/10/21 19:09:35 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/21 19:08:17] ( ALLERESET )
       OPERATION2:[21/10/21 19:08:06] ( ALLERESET )
       OPERATION3:[21/10/21 18:59:12] ( here home1 )
       OPERATION4:[21/10/21 18:57:32] ( Motor power ON )
       OPERATION5:[21/10/21 18:56:53] ( REPEAT->TEACH )
       OPERATION6:[21/10/21 18:55:57] ( SAVE using.rcc )
       OPERATION7:[21/10/21 18:55:44] ( SAVE using.rcc )
       OPERATION8:[21/10/21 18:55:40] ( ID )
       OPERATION9:[21/10/21 18:55:40] ( TYPE DEXT(HERE,9) )
       ROBOT1:
        PROGRAM:home Step:0 Cur_Step:31 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         19.390    48.318   -37.300    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      10413.301

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         19.390    48.318   -37.300    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

      10413.301

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

          0.000    -0.001     0.000    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
 105 - [21/10/21 19:08:15 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/21 19:08:06] ( ALLERESET )
       OPERATION2:[21/10/21 18:59:12] ( here home1 )
       OPERATION3:[21/10/21 18:57:32] ( Motor power ON )
       OPERATION4:[21/10/21 18:56:53] ( REPEAT->TEACH )
       OPERATION5:[21/10/21 18:55:57] ( SAVE using.rcc )
       OPERATION6:[21/10/21 18:55:44] ( SAVE using.rcc )
       OPERATION7:[21/10/21 18:55:40] ( ID )
       OPERATION8:[21/10/21 18:55:40] ( TYPE DEXT(HERE,9) )
       OPERATION9:[21/10/21 18:55:40] ( TYPE DEXT(HERE,8) )
       ROBOT1:
        PROGRAM:home Step:0 Cur_Step:31 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 101 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         19.390    48.318   -37.300    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       9347.101

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         19.390    48.318   -37.300    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       9347.101

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

          0.000    -0.001     0.000    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
 106 - [21/10/21 19:08:01 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (E6010)Out of XYZ MOVING AREA LIMIT.
       OPERATION1:[21/10/21 18:59:12] ( here home1 )
       OPERATION2:[21/10/21 18:57:32] ( Motor power ON )
       OPERATION3:[21/10/21 18:56:53] ( REPEAT->TEACH )
       OPERATION4:[21/10/21 18:55:57] ( SAVE using.rcc )
       OPERATION5:[21/10/21 18:55:44] ( SAVE using.rcc )
       OPERATION6:[21/10/21 18:55:40] ( ID )
       OPERATION7:[21/10/21 18:55:40] ( TYPE DEXT(HERE,9) )
       OPERATION8:[21/10/21 18:55:40] ( TYPE DEXT(HERE,8) )
       OPERATION9:[21/10/21 18:55:40] ( TYPE DEXT(HERE,7) )
       ROBOT1:
        PROGRAM:home Step:0 Cur_Step:31 STATUS:STOP
       PC1 PROGRAM: string_window Step No: 134 STATUS: RUN
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         19.390    48.318   -37.300    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       9347.101

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

         19.390    48.318   -37.300    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

       9347.101

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

          0.000    -0.001     0.000    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.400

------------------------------------------------------------------------------
 107 - [21/10/21 18:41:25 SIGNAL:00 MON.SPEED : 0 REPEAT mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/21 18:41:07] ( CYCLE START )
       OPERATION2:[21/10/21 18:41:00] ( Motor power ON )
       OPERATION3:[21/10/21 18:40:58] ( CYCLE START )
       OPERATION4:[21/10/21 18:40:38] ( CYCLE START )
       OPERATION5:[21/10/21 18:40:29] ( CYCLE START )
       OPERATION6:[21/10/21 18:39:40] ( TEACH->REPEAT )
       OPERATION7:[21/10/21 18:36:43] ( vis_measure_no = 3 )
       OPERATION8:[21/10/21 18:16:38] ( khidl1 disconnect )
       OPERATION9:[21/10/21 18:16:16] ( SAVE using.rcc )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000  5130.416

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        161.700

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000  5130.416

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        161.700

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000  5130.416

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        161.700

------------------------------------------------------------------------------
 108 - [21/10/21 16:14:52 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/21 15:49:06] ( EMERGENCY STOP )
       OPERATION2:[21/10/21 15:49:04] ( Control Power ON )
       OPERATION3:[21/10/21 15:49:02] ( webml1 connect )
       OPERATION4:[21/10/21 15:10:28] ( EMERGENCY STOP )
       OPERATION5:[21/10/21 15:10:25] ( Control Power ON )
       OPERATION6:[21/10/21 15:10:24] ( webml1 connect )
       OPERATION7:[21/10/20 23:51:49] ( ERESET 1: )
       OPERATION8:[21/10/20 23:41:44] ( EMERGENCY STOP )
       OPERATION9:[21/10/20 23:41:42] ( Control Power ON )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 109 - [21/10/21 15:48:28 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/21 15:10:28] ( EMERGENCY STOP )
       OPERATION2:[21/10/21 15:10:25] ( Control Power ON )
       OPERATION3:[21/10/21 15:10:24] ( webml1 connect )
       OPERATION4:[21/10/20 23:51:49] ( ERESET 1: )
       OPERATION5:[21/10/20 23:41:44] ( EMERGENCY STOP )
       OPERATION6:[21/10/20 23:41:42] ( Control Power ON )
       OPERATION7:[21/10/20 23:41:40] ( webml1 connect )
       OPERATION8:[21/10/20 23:34:50] ( as )
       OPERATION9:[21/10/20 23:34:25] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 110 - [21/10/21 00:06:42 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/20 23:51:49] ( ERESET 1: )
       OPERATION2:[21/10/20 23:41:44] ( EMERGENCY STOP )
       OPERATION3:[21/10/20 23:41:42] ( Control Power ON )
       OPERATION4:[21/10/20 23:41:40] ( webml1 connect )
       OPERATION5:[21/10/20 23:34:50] ( as )
       OPERATION6:[21/10/20 23:34:25] ( EMERGENCY STOP )
       OPERATION7:[21/10/20 23:34:22] ( Control Power ON )
       OPERATION8:[21/10/20 23:34:21] ( webml1 connect )
       OPERATION9:[21/10/20 23:32:12] ( EMERGENCY STOP )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

------------------------------------------------------------------------------
 111 - [21/10/20 23:41:10 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/20 23:34:50] ( as )
       OPERATION2:[21/10/20 23:34:25] ( EMERGENCY STOP )
       OPERATION3:[21/10/20 23:34:22] ( Control Power ON )
       OPERATION4:[21/10/20 23:34:21] ( webml1 connect )
       OPERATION5:[21/10/20 23:32:12] ( EMERGENCY STOP )
       OPERATION6:[21/10/20 23:32:09] ( Control Power ON )
       OPERATION7:[21/10/20 23:32:08] ( webml1 connect )
       OPERATION8:[21/10/20 23:24:30] ( password = 1234 )
       OPERATION9:[21/10/20 23:22:24] ( khidl1 disconnect )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 112 - [21/10/20 23:33:50 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/20 23:32:12] ( EMERGENCY STOP )
       OPERATION2:[21/10/20 23:32:09] ( Control Power ON )
       OPERATION3:[21/10/20 23:32:08] ( webml1 connect )
       OPERATION4:[21/10/20 23:24:30] ( password = 1234 )
       OPERATION5:[21/10/20 23:22:24] ( khidl1 disconnect )
       OPERATION6:[21/10/20 23:22:00] ( SAVE using.rcc )
       OPERATION7:[21/10/20 23:18:40] ( password = 1234 )
       OPERATION8:[21/10/20 23:18:26] ( EMERGENCY STOP )
       OPERATION9:[21/10/20 23:18:20] ( PCEXECUTE 1: autos )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: STOP
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: STOP
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 113 - [21/10/20 23:31:37 SIGNAL:00 MON.SPEED : 0 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/20 23:24:30] ( password = 1234 )
       OPERATION2:[21/10/20 23:22:24] ( khidl1 disconnect )
       OPERATION3:[21/10/20 23:22:00] ( SAVE using.rcc )
       OPERATION4:[21/10/20 23:18:40] ( password = 1234 )
       OPERATION5:[21/10/20 23:18:26] ( EMERGENCY STOP )
       OPERATION6:[21/10/20 23:18:20] ( PCEXECUTE 1: autos )
       OPERATION7:[21/10/20 23:18:08] ( DIRECTORY/N/P/CMT  )
       OPERATION8:[21/10/20 23:18:07] ( DIRECTORY/SIZ )
       OPERATION9:[21/10/20 23:17:29] ( password = 1234 )
       ROBOT1:
        PROGRAM:main Step:0 Cur_Step:1 STATUS:STOP
       PC1 PROGRAM: autostart.pc Step No: 89 STATUS: WAIT
       PC2 PROGRAM: feed_in_con.pc Step No: 17 STATUS: WAIT
       PC3 PROGRAM: feed_out_con.pc Step No: 22 STATUS: WAIT
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.500

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.500

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000     0.000

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

        278.500

------------------------------------------------------------------------------
 114 - [21/10/20 23:03:18 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/20 22:58:50] ( as )
       OPERATION2:[21/10/20 22:58:25] ( EMERGENCY STOP )
       OPERATION3:[21/10/20 22:58:22] ( Control Power ON )
       OPERATION4:[21/10/20 22:58:21] ( webml1 connect )
       OPERATION5:[21/10/20 22:57:39] ( ALLERESET )
       OPERATION6:[21/10/20 22:45:42] ( EMERGENCY STOP )
       OPERATION7:[21/10/20 22:45:39] ( Control Power ON )
       OPERATION8:[21/10/20 22:45:38] ( webml1 connect )
       OPERATION9:[21/10/20 22:38:35] ( PRIME/C 1:autostar )
       ROBOT1:
        PROGRAM:autostart.pc Step:0 Cur_Step:1 STATUS:STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 115 - [21/10/20 22:57:49 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/20 22:57:39] ( ALLERESET )
       OPERATION2:[21/10/20 22:45:42] ( EMERGENCY STOP )
       OPERATION3:[21/10/20 22:45:39] ( Control Power ON )
       OPERATION4:[21/10/20 22:45:38] ( webml1 connect )
       OPERATION5:[21/10/20 22:38:35] ( PRIME/C 1:autostar )
       OPERATION6:[21/10/20 22:38:26] ( DIRECTORY/N/P/CMT  )
       OPERATION7:[21/10/20 22:38:26] ( DIRECTORY/SIZ )
       OPERATION8:[21/10/20 22:38:20] ( ERESET 1: )
       OPERATION9:[21/10/20 22:37:47] ( as )
       ROBOT1:
        PROGRAM:autostart.pc Step:0 Cur_Step:1 STATUS:STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

          0.000

------------------------------------------------------------------------------
 116 - [21/10/20 22:45:03 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/20 22:38:35] ( PRIME/C 1:autostar )
       OPERATION2:[21/10/20 22:38:26] ( DIRECTORY/N/P/CMT  )
       OPERATION3:[21/10/20 22:38:26] ( DIRECTORY/SIZ )
       OPERATION4:[21/10/20 22:38:20] ( ERESET 1: )
       OPERATION5:[21/10/20 22:37:47] ( as )
       OPERATION6:[21/10/20 22:37:22] ( EMERGENCY STOP )
       OPERATION7:[21/10/20 22:37:19] ( Control Power ON )
       OPERATION8:[21/10/20 22:37:18] ( webml1 connect )
       OPERATION9:[21/10/20 22:36:23] ( ERESET 1: )
       ROBOT1:
        PROGRAM:autostart.pc Step:0 Cur_Step:1 STATUS:STOP
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 117 - [21/10/20 22:36:47 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/20 22:36:23] ( ERESET 1: )
       OPERATION2:[21/10/20 22:35:48] ( LOAD using.rcc )
       OPERATION3:[21/10/20 22:35:11] ( LOAD using.rcc )
       OPERATION4:[21/10/20 22:34:56] ( LOAD using.rcc )
       OPERATION5:[21/10/20 22:34:48] ( LOAD using.rcc )
       OPERATION6:[21/10/20 22:34:39] ( LOAD using.rcc )
       OPERATION7:[21/10/20 22:34:30] ( LOAD using.rcc )
       OPERATION8:[21/10/20 22:34:12] ( LOAD using.rcc )
       OPERATION9:[21/10/20 22:34:12] ( TYPE TASK (1) )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 118 - [21/10/20 22:08:17 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/20 21:55:12] ( AUX1 connect )
       OPERATION2:[21/10/20 15:37:54] ( EMERGENCY STOP )
       OPERATION3:[21/10/20 15:37:51] ( Control Power ON )
       OPERATION4:[21/10/20 15:37:50] ( webml1 connect )
       OPERATION5:[21/10/19 18:28:13] ( EMERGENCY STOP )
       OPERATION6:[21/10/19 18:28:11] ( Control Power ON )
       OPERATION7:[21/10/19 18:28:09] ( webml1 connect )
       OPERATION8:[21/10/19 17:34:23] ( EMERGENCY STOP )
       OPERATION9:[21/10/19 17:34:20] ( Control Power ON )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 119 - [21/10/20 01:45:08 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/19 18:28:13] ( EMERGENCY STOP )
       OPERATION2:[21/10/19 18:28:11] ( Control Power ON )
       OPERATION3:[21/10/19 18:28:09] ( webml1 connect )
       OPERATION4:[21/10/19 17:34:23] ( EMERGENCY STOP )
       OPERATION5:[21/10/19 17:34:20] ( Control Power ON )
       OPERATION6:[21/10/19 17:34:19] ( webml1 connect )
       OPERATION7:[21/10/19 16:47:27] ( EMERGENCY STOP )
       OPERATION8:[21/10/19 16:47:24] ( Control Power ON )
       OPERATION9:[21/10/19 16:47:23] ( webml1 connect )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 120 - [21/10/19 18:27:23 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/19 17:34:23] ( EMERGENCY STOP )
       OPERATION2:[21/10/19 17:34:20] ( Control Power ON )
       OPERATION3:[21/10/19 17:34:19] ( webml1 connect )
       OPERATION4:[21/10/19 16:47:27] ( EMERGENCY STOP )
       OPERATION5:[21/10/19 16:47:24] ( Control Power ON )
       OPERATION6:[21/10/19 16:47:23] ( webml1 connect )
       OPERATION7:[21/10/19 16:02:05] ( EMERGENCY STOP )
       OPERATION8:[21/10/19 16:02:02] ( Control Power ON )
       OPERATION9:[21/10/19 16:02:01] ( webml1 connect )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 121 - [21/10/19 17:29:23 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/19 16:47:27] ( EMERGENCY STOP )
       OPERATION2:[21/10/19 16:47:24] ( Control Power ON )
       OPERATION3:[21/10/19 16:47:23] ( webml1 connect )
       OPERATION4:[21/10/19 16:02:05] ( EMERGENCY STOP )
       OPERATION5:[21/10/19 16:02:02] ( Control Power ON )
       OPERATION6:[21/10/19 16:02:01] ( webml1 connect )
       OPERATION7:[21/10/19 15:27:56] ( EMERGENCY STOP )
       OPERATION8:[21/10/19 15:27:53] ( Control Power ON )
       OPERATION9:[21/10/19 15:27:52] ( webml1 connect )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 122 - [21/10/19 16:35:44 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/19 16:02:05] ( EMERGENCY STOP )
       OPERATION2:[21/10/19 16:02:02] ( Control Power ON )
       OPERATION3:[21/10/19 16:02:01] ( webml1 connect )
       OPERATION4:[21/10/19 15:27:56] ( EMERGENCY STOP )
       OPERATION5:[21/10/19 15:27:53] ( Control Power ON )
       OPERATION6:[21/10/19 15:27:52] ( webml1 connect )
       OPERATION7:[21/10/18 21:57:12] ( EMERGENCY STOP )
       OPERATION8:[21/10/18 21:52:28] ( EMERGENCY STOP )
       OPERATION9:[21/10/18 21:31:20] ( EMERGENCY STOP )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 123 - [21/10/19 15:34:08 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/19 15:27:56] ( EMERGENCY STOP )
       OPERATION2:[21/10/19 15:27:53] ( Control Power ON )
       OPERATION3:[21/10/19 15:27:52] ( webml1 connect )
       OPERATION4:[21/10/18 21:57:12] ( EMERGENCY STOP )
       OPERATION5:[21/10/18 21:52:28] ( EMERGENCY STOP )
       OPERATION6:[21/10/18 21:31:20] ( EMERGENCY STOP )
       OPERATION7:[21/10/18 21:16:48] ( EMERGENCY STOP )
       OPERATION8:[21/10/18 21:16:14] ( EMERGENCY STOP )
       OPERATION9:[21/10/18 20:58:04] ( EMERGENCY STOP )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 124 - [21/10/18 22:41:12 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/18 21:57:12] ( EMERGENCY STOP )
       OPERATION2:[21/10/18 21:52:28] ( EMERGENCY STOP )
       OPERATION3:[21/10/18 21:31:20] ( EMERGENCY STOP )
       OPERATION4:[21/10/18 21:16:48] ( EMERGENCY STOP )
       OPERATION5:[21/10/18 21:16:14] ( EMERGENCY STOP )
       OPERATION6:[21/10/18 20:58:04] ( EMERGENCY STOP )
       OPERATION7:[21/10/18 20:53:43] ( EMERGENCY STOP )
       OPERATION8:[21/10/18 20:53:40] ( Control Power ON )
       OPERATION9:[21/10/18 20:53:39] ( webml1 connect )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 125 - [21/10/18 20:49:47 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/18 20:49:22] ( Control Power ON )
       OPERATION2:[21/10/18 20:49:21] ( webml1 connect )
       OPERATION3:[21/10/18 19:09:56] ( EMERGENCY STOP )
       OPERATION4:[21/10/18 18:58:41] ( as )
       OPERATION5:[21/10/18 18:58:16] ( EMERGENCY STOP )
       OPERATION6:[21/10/18 18:58:13] ( Control Power ON )
       OPERATION7:[21/10/18 18:58:12] ( webml1 connect )
       OPERATION8:[21/10/18 18:13:39] ( EMERGENCY STOP )
       OPERATION9:[21/10/18 18:12:41] ( EMERGENCY STOP )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 126 - [21/10/18 20:29:05 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/18 19:09:56] ( EMERGENCY STOP )
       OPERATION2:[21/10/18 18:58:41] ( as )
       OPERATION3:[21/10/18 18:58:16] ( EMERGENCY STOP )
       OPERATION4:[21/10/18 18:58:13] ( Control Power ON )
       OPERATION5:[21/10/18 18:58:12] ( webml1 connect )
       OPERATION6:[21/10/18 18:13:39] ( EMERGENCY STOP )
       OPERATION7:[21/10/18 18:12:41] ( EMERGENCY STOP )
       OPERATION8:[21/10/18 18:12:28] ( EMERGENCY STOP )
       OPERATION9:[21/10/18 18:12:25] ( Control Power ON )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 127 - [21/10/18 18:52:07 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/18 18:13:39] ( EMERGENCY STOP )
       OPERATION2:[21/10/18 18:12:41] ( EMERGENCY STOP )
       OPERATION3:[21/10/18 18:12:28] ( EMERGENCY STOP )
       OPERATION4:[21/10/18 18:12:25] ( Control Power ON )
       OPERATION5:[21/10/18 18:12:24] ( webml1 connect )
       OPERATION6:[21/10/18 17:57:50] ( EMERGENCY STOP )
       OPERATION7:[21/10/18 17:57:47] ( Control Power ON )
       OPERATION8:[21/10/18 17:57:46] ( webml1 connect )
       OPERATION9:[21/10/18 17:30:54] ( EMERGENCY STOP )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 128 - [21/10/18 18:05:55 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/18 17:57:50] ( EMERGENCY STOP )
       OPERATION2:[21/10/18 17:57:47] ( Control Power ON )
       OPERATION3:[21/10/18 17:57:46] ( webml1 connect )
       OPERATION4:[21/10/18 17:30:54] ( EMERGENCY STOP )
       OPERATION5:[21/10/18 17:30:51] ( Control Power ON )
       OPERATION6:[21/10/18 17:30:50] ( webml1 connect )
       OPERATION7:[21/10/18 01:49:43] ( as )
       OPERATION8:[21/10/18 01:49:18] ( EMERGENCY STOP )
       OPERATION9:[21/10/18 01:49:15] ( Control Power ON )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 129 - [21/10/18 17:45:11 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/18 17:30:54] ( EMERGENCY STOP )
       OPERATION2:[21/10/18 17:30:51] ( Control Power ON )
       OPERATION3:[21/10/18 17:30:50] ( webml1 connect )
       OPERATION4:[21/10/18 01:49:43] ( as )
       OPERATION5:[21/10/18 01:49:18] ( EMERGENCY STOP )
       OPERATION6:[21/10/18 01:49:15] ( Control Power ON )
       OPERATION7:[21/10/18 01:49:14] ( webml1 connect )
       OPERATION8:[21/10/18 00:49:04] ( EMERGENCY STOP )
       OPERATION9:[21/10/18 00:49:02] ( Control Power ON )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 130 - [21/10/18 01:56:44 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/18 01:49:43] ( as )
       OPERATION2:[21/10/18 01:49:18] ( EMERGENCY STOP )
       OPERATION3:[21/10/18 01:49:15] ( Control Power ON )
       OPERATION4:[21/10/18 01:49:14] ( webml1 connect )
       OPERATION5:[21/10/18 00:49:04] ( EMERGENCY STOP )
       OPERATION6:[21/10/18 00:49:02] ( Control Power ON )
       OPERATION7:[21/10/18 00:49:00] ( webml1 connect )
       OPERATION8:[21/10/15 00:04:38] ( EMERGENCY STOP )
       OPERATION9:[21/10/15 00:04:36] ( Control Power ON )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 131 - [21/10/18 01:18:22 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/18 00:49:04] ( EMERGENCY STOP )
       OPERATION2:[21/10/18 00:49:02] ( Control Power ON )
       OPERATION3:[21/10/18 00:49:00] ( webml1 connect )
       OPERATION4:[21/10/15 00:04:38] ( EMERGENCY STOP )
       OPERATION5:[21/10/15 00:04:36] ( Control Power ON )
       OPERATION6:[21/10/15 00:04:34] ( webml1 connect )
       OPERATION7:[21/10/14 23:40:05] ( as )
       OPERATION8:[21/10/14 23:39:40] ( EMERGENCY STOP )
       OPERATION9:[21/10/14 23:39:37] ( Control Power ON )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.987   -27.985    -0.207     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 132 - [21/10/15 00:20:50 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/15 00:04:38] ( EMERGENCY STOP )
       OPERATION2:[21/10/15 00:04:36] ( Control Power ON )
       OPERATION3:[21/10/15 00:04:34] ( webml1 connect )
       OPERATION4:[21/10/14 23:40:05] ( as )
       OPERATION5:[21/10/14 23:39:40] ( EMERGENCY STOP )
       OPERATION6:[21/10/14 23:39:37] ( Control Power ON )
       OPERATION7:[21/10/14 23:39:36] ( webml1 connect )
       OPERATION8:[21/10/14 23:07:19] ( EMERGENCY STOP )
       OPERATION9:[21/10/14 23:07:16] ( Control Power ON )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 133 - [21/10/14 23:40:47 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/14 23:40:05] ( as )
       OPERATION2:[21/10/14 23:39:40] ( EMERGENCY STOP )
       OPERATION3:[21/10/14 23:39:37] ( Control Power ON )
       OPERATION4:[21/10/14 23:39:36] ( webml1 connect )
       OPERATION5:[21/10/14 23:07:19] ( EMERGENCY STOP )
       OPERATION6:[21/10/14 23:07:16] ( Control Power ON )
       OPERATION7:[21/10/14 23:07:15] ( webml1 connect )
       OPERATION8:[21/10/14 22:52:45] ( as )
       OPERATION9:[21/10/14 22:52:20] ( EMERGENCY STOP )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 134 - [21/10/14 23:08:01 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/14 23:07:19] ( EMERGENCY STOP )
       OPERATION2:[21/10/14 23:07:16] ( Control Power ON )
       OPERATION3:[21/10/14 23:07:15] ( webml1 connect )
       OPERATION4:[21/10/14 22:52:45] ( as )
       OPERATION5:[21/10/14 22:52:20] ( EMERGENCY STOP )
       OPERATION6:[21/10/14 22:52:17] ( Control Power ON )
       OPERATION7:[21/10/14 22:52:16] ( webml1 connect )
       OPERATION8:[21/10/14 22:43:20] ( EMERGENCY STOP )
       OPERATION9:[21/10/14 22:43:18] ( Control Power ON )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 135 - [21/10/14 23:05:54 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/14 22:52:45] ( as )
       OPERATION2:[21/10/14 22:52:20] ( EMERGENCY STOP )
       OPERATION3:[21/10/14 22:52:17] ( Control Power ON )
       OPERATION4:[21/10/14 22:52:16] ( webml1 connect )
       OPERATION5:[21/10/14 22:43:20] ( EMERGENCY STOP )
       OPERATION6:[21/10/14 22:43:18] ( Control Power ON )
       OPERATION7:[21/10/14 22:43:16] ( webml1 connect )
       OPERATION8:[21/02/22 14:53:19] ( CARD_SAVE USB\y030 )
       OPERATION9:[21/02/22 14:52:58] ( sa/hsp y030561 )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 136 - [21/10/14 22:45:20 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/10/14 22:43:20] ( EMERGENCY STOP )
       OPERATION2:[21/10/14 22:43:18] ( Control Power ON )
       OPERATION3:[21/10/14 22:43:16] ( webml1 connect )
       OPERATION4:[21/02/22 14:53:19] ( CARD_SAVE USB\y030 )
       OPERATION5:[21/02/22 14:52:58] ( sa/hsp y030561 )
       OPERATION6:[21/02/22 14:52:47] ( zclre )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.985   -27.991   -28.001    -1.772     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
 137 - [21/02/22 14:53:47 SIGNAL:00 MON.SPEED : 10 TEACH mode]
       (D1561)[Power sequence board]AC primary power OFF.
       OPERATION1:[21/02/22 14:53:19] ( CARD_SAVE USB\y030 )
       OPERATION2:[21/02/22 14:52:58] ( sa/hsp y030561 )
       OPERATION3:[21/02/22 14:52:47] ( zclre )
       OPERATION8:[70/01/01 00:43:28] ( ENGLISH )
       Current Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.997   -27.997   -27.997     0.071     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       Command Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.997   -27.997   -27.997     0.071     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

       End Pose
         JT1       JT2       JT3       JT4       JT5       JT6       CONV1         

        -27.997   -27.997   -27.997     0.071     0.000     0.000-134217728

         CONV2     JT9       JT10      JT11      JT12      JT13      JT14 

     -134217728.000

------------------------------------------------------------------------------
.END
